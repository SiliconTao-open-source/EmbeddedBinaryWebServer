#include "WebObjects.h"
#include <PageServer.h>
#include <STHash.h>
#include <stdio.h>
#include <STString.h>
#include <STDriveOps.h>
#include <STStringList.h>

WebObjects::WebObjects() 
{
}
	
WebObjects::~WebObjects()
{
	// printf("WebObjects::~WebObjects() @ 0x%08X\n", this);
}		
	
int WebObjects::ObjectEntry(void* xPageContent, void* PGlobalVars)
{
	PageContent = (PageServer *)xPageContent;
	GlobalVars = (STHash *)PGlobalVars;
	_SERVER = (STHash*)(*GlobalVars)["_SERVER"].ToInt();
	_GET = (STHash*)(*GlobalVars)["_GET"].ToInt();
	_POST = (STHash*)(*GlobalVars)["_POST"].ToInt();
	_COOKIE = (STHash*)(*GlobalVars)["_COOKIE"].ToInt();
	_GLOBAL = (STHash*)(*GlobalVars)["_GLOBAL"].ToInt();
	_FILE = (STHash*)(*GlobalVars)["_FILE"].ToInt();
	return(0); // The PageObject should inherret this member and return an error code.
}

/*void WebObjects::ObjectExit()
{
	Close();
}*/

void WebObjects::SetContentType(const STString& CustomContentType)
{
	PageContent->SetContentType(CustomContentType);	
}

void WebObjects::SetContentType(const char* CustomContentType)
{
	PageContent->SetContentType(CustomContentType);
}

void WebObjects::EvalFile(const char* FileName)
{
	STString TempStr = FileName;
	EvalFile(TempStr);
}

void WebObjects::EvalPosted()
{
	PageContent->EvalPosted();
}

void WebObjects::Endorse(STHash *HashPointer, const char *HashName)
{
	STString TempStr = HashName;
	Endorse(HashPointer, TempStr);
}

void WebObjects::Endorse(STHash *HashPointer, const STString& HashName)
{
	PageContent->Endorse(HashPointer, HashName);
}

void WebObjects::Disavow(const char *HashName)
{
	PageContent->Disavow(HashName);
}

void WebObjects::EvalFile(const STString& FileName)
{
	PageContent->EvalFile(FileName);
}

void WebObjects::HttpError(int NewError)
{
	PageContent->SetError(NewError);
}

void WebObjects::HPost(const char *PostString)
{
	STString TempStr = PostString;
	HPost(TempStr);
}

void WebObjects::HPost(const STString& PostString)
{
	PageContent->HPost(PostString);
}

void WebObjects::IPost(const char *PostString)
{
	STString TempStr = PostString;
	IPost(TempStr);
}

void WebObjects::IPost(const STString& PostString)
{
	PageContent->IPost(PostString);
}

int WebObjects::ExecuteLibrary(const STString& LibRelPathName)
{
	STString WorkStr = LibRelPathName;
	WorkStr += ".so";
	return(PageContent->ExecuteLibrary(WorkStr));
}

void WebObjects::Post(const char *PostString)
{
	STString TempStr = PostString;
	Post(TempStr);
}

void WebObjects::Post(const STString& PostString)
{
	PageContent->Post(PostString);
}

void WebObjects::ShowVars()
{
	PageContent->ShowVars();
}

void WebObjects::PostFromFilePre(const char *FileName)
{
	STString TempStr = FileName;
	PostFromFilePre(TempStr);	
}

void WebObjects::PostFromFilePre(const STString& FileName)
{
	PageContent->PostFromFilePre(FileName);
}

void WebObjects::PostFromFile(const char *FileName)
{
	STString TempStr = FileName;
	PostFromFile(TempStr);
}

void WebObjects::PostFromFile(const STString& FileName)
{
	PageContent->PostFromFile(FileName);
}

void WebObjects::EPost(const char *PostString)
{
	STString TempStr = PostString;
	EPost(TempStr);
}

void WebObjects::EPost(const STString& PostString)
{
	PageContent->EPost(PostString);
}

void WebObjects::SetCookie(const char *CookieString)
{
	PageContent->SetCookie(CookieString);
}

void WebObjects::SetCookie(const STString& CookieString)
{
	PageContent->SetCookie(CookieString);
}

void WebObjects::SetCookie(const char *format, ...)
{
   va_list argp;  // Create an argument list pointer
	va_start(argp, format);	
	// PageContent->SetCookie(format, &argp);
	STString CookieStr;
	CookieStr.VaListFormating(format, &argp);
	PageContent->SetCookie(CookieStr);
	va_end(argp);
}

STDynamicDataLink* WebObjects::GetDataLink()
{
	return(PageContent->GetDataLink());
}

bool WebObjects::FileUploaded()
{
	return(PageContent->FileUploaded());
}

bool WebObjects::SaveUploadedFile(const STString& NewFilePathName)
{
	return(PageContent->SaveUploadedFile(NewFilePathName));
}

void WebObjects::Log(const char* NewLogMessage, ...)
{
   va_list argp;  // Create an argument list pointer
	va_start(argp, NewLogMessage);
	STString LogMsg;	
	LogMsg.VaListFormating(NewLogMessage, &argp);
	PageContent->Log((const char*)LogMsg);
	va_end(argp);
}
