extern "C" 
{
   // To initialize the library and create the object and return the object pointer.
   void* Initialize();			

	// Object entry. This is where a call to the web page will enter.
   int ObjectEntry(void* po, void* PageContent, void* PGlobalVars);
	
	// Disconects from the object so that it can be unloaded and removed from memorey.
	void Close(void* po);
};
