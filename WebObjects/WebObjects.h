#ifndef included_WebObjects
#define included_WebObjects

class PageServer;
class STHash;

#include <STString.h>

class STDynamicDataLink;

/**
 * WebObjects is the entry point for the EBWS library API. This object should be compiled and linked to your PageObject file. Use a symbolic link in your source directory to allow this file to compile correctly.
 * 
 * WebObjects comands are avialbe from your PageObject because your PageObject must inherret this object. Your PageObject file must also contain a memberfunction Main() and a member funciton Close().
 * 
 * Main() is the entry poing that WebObjects uses to process calls to your PageObject.
 * 
 * Close() is called by EBWS via WebObjects to unload any variable and close any sockets that you may have kept open. You can open a connection to a library or file and keep it open while EBWS is running
 * A good example of this would be a connection to an SQL database sever. When you make a connection to the database server you can keep the pointer to the instance in a global variable.
 * 
 * Global variables are kept in an array of STHash's and can be accessed inside PageObject as a referenct to the STHash.
 * Example
 * <PRE>
 * STString LoginName = (*_GET)["username"];
 * </PRE>
 * 
 * Standard global variables.
 * *_SERVER, *_GET, *_POST, *_COOKIE, *_GLOBAL, *_FILE
 * 
 * For more information see /var/www/About_Ebws.html
 * or
 * http://www.silicontao.com/software/ebws/About_Ebws.html
 */


class WebObjects
{
	public:
		/**
		 * Set a custom content-type in the HPost.
		 */
		void SetContentType(const STString& CustomContentType);
		void SetContentType(const char* CustomContentType);	

		/** 
	    * Post text as regular HTML to the client browser.
		 */ 
		void Post(const char *PostString);
		void Post(const STString& PostString);

		/** 
		 *	Post extra information about the error after an error has happened in the same page load event. You must first call @ref HttpError to set the HTML error code. See also Error Codes and SetError.
	    */
		void EPost(const char *PostString);
		void EPost(const STString& PostString);

		/** 
	    * Sets the HTML error code value. Use @ref to set the HTML error message.
		 */ 
		void HttpError(int NewError);

		/** 
	    * Posts the contents of a file. Wraps it in HTML standard tags. Only useful if the file is an HTML formated file. For non-HTML files use @ref PostFromFilePre
		 */ 
		void PostFromFile(const char *FileName);
		void PostFromFile(const STString& FileName);

		/** 
	    * Simple API to show the contents of the EBWS system variables.
		 */ 
		void ShowVars();

		/**
		 * Works like @ref PostFromFile but parses the file for known global variables and replaces them with known values. Must use @ref Endorse to register custom variables first.
		 */
		void EvalFile(const char* FileName);
		void EvalFile(const STString& FileName);
		
		/**
		 * Works like @ref EvalFile but parses posted data in memory buffer that is ready to be sent out to the client.
		 */
		void EvalPosted();

		/**
		 *	Post normal text as HTML header. Format is set to HTML.
	    */	
		void HPost(const STString& PostString);
		void HPost(const char *PostString);

		/**
		 *	Post normal text as included. Sets format to included so no formating data is sent.
	    */	
		void IPost(const STString& PostString);
		void IPost(const char *PostString);

		/**
		 * Displays the content of static text file as an HTML formated page to the viewing client.
		 */
		void PostFromFilePre(const char *FileName);
		void PostFromFilePre(const STString& FileName);

		/**
		 * Required to create and use custom STHash variables globally. Hash variables must be endorsed before they will be identified by EvalFile.
		 */
		void Endorse(STHash *HashPointer, const STString& HashName);
		void Endorse(STHash *HashPointer, const char *HashName);

		/** 
		 * Used to remove global STHash variables when they are no longer needed.
		 */
		void Disavow(const char *HashName);

		/**
		 *	Used to set a cookie on the client browser. This function has no smarts to help with proper cookie formating. You must give it a complete cookie string that is well formated.
		 * @li
		 * Example call
		 * <PRE>
		 * SetCookie("field3=My+domain+is+very+cool; expires=Wed, 03 Oct 2007 02:13:49 GMT; path=/path/to/thing/; domain= www.SiliconTao.com");
		 * </PRE>
		 */
		void SetCookie(const char *CookieString);
		void SetCookie(const STString& CookieString);
		void SetCookie(const char *format, ...);	

		/**
		 * Returns a pointer the STDynamicDataLink that is running in EBWS. Only one instance of STDynamicDataLink is able to run per program. EBWS provides
		 * this service to enable inter application and system communication. This link is not serviced very often so it can only be used to ask questions. 
		 * If the DataLink is never used it will not be loaded, it will only be loaded the first time a question is asked.
		 */
		STDynamicDataLink* GetDataLink();

		/**
		 * EBWS currently supports the FORM uploading of one file at a time. This function returns true if a file was uploaded by the viewing client.
		 * Look to the values stored in the global variable _FILE to find details about the uploaded file.
		 */
		bool FileUploaded();

		/**
		 * Saves the uploaded file to the giving file name and clears and frees memory used by the global variable _FILE.
		 */
		bool SaveUploadedFile(const STString& NewFilePathName);

		/**
		 * Log messages to the syslogd server. If EBWS is run in debug mode text is also output to STDOUT.
		 */
		void Log(const char*NewLogMessage, ...);

		/**
		 * Loads a shared object file of a given name then executes standard EBWS calls to it. Can retrun an int but it has a custom meaning not an error value.
		 */
		int ExecuteLibrary(const STString& LibRelPathName);

		/** 
	    * These required member functions are for EBWS library entry. You should not need to use them directly.
		 */ 
		WebObjects();
		~WebObjects();	
		int ObjectEntry(void* xPageContent, void* PGlobalVars);

	protected:
		PageServer *PageContent;
		STHash *GlobalVars, *_SERVER, *_GET, *_POST, *_COOKIE, *_GLOBAL, *_FILE;
};


#endif // included_WebObjects
