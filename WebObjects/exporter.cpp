#include "exporter.h"
#include "PageObject.h"
#include <stdio.h>

// You as the developer write an object called PageObject and make in inherit from WebObject

void* Initialize()
{
	PageObject* po = new PageObject();
	return((void*)po);	
}

int ObjectEntry(void* po, void* PageContent, void* PGlobalVars)
{
	PageObject* Pop = (PageObject*)po;
	Pop->ObjectEntry(PageContent, PGlobalVars);
	return(Pop->Main());
}

void Close(void* po)
{
	PageObject* Pop = (PageObject*)po;
	Pop->Close();
	delete Pop;
}
