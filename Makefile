# To limit the compile to a select group of sub projects or to specifiy the order they compile in set PROJECTS: value in the .make.in file.
PROJECTS:=$(shell cat .make.in | grep ^PROJECTS | cut -d: -f2- 2>/dev/null)
ALL_MAKES:=$(shell find . -name Makefile -exec dirname "{}" \; | cut -d/ -f2 | uniq | grep -v "\."$$ )
VERSION=0.9.0

ifeq ($(PROJECTS),)
	PROJECTS:=$(ALL_MAKES)
endif
ORIG_DIR = $(shell pwd)

$(MAKECMDGOALS):
	@if test -d $(MAKECMDGOALS); then cd $(MAKECMDGOALS); make; fi
	
all:
	@$(foreach dir, $(PROJECTS), echo "------ Make $(dir) ------"; cd $(dir); make; [ $$? = 0 ] || exit 1; cd -;)

clean:
	@$(foreach dir, $(ALL_MAKES), echo "------ Clean $(dir) ------"; cd $(dir); make clean; cd -;)

install:
	@$(foreach dir, $(PROJECTS), echo "------ Install $(dir) ------"; cd $(dir); make install; [ $$? = 0 ] || exit 1; cd -;)

package:
	@$(foreach dir, $(PROJECTS), echo "------ Package $(dir) ------"; cd $(dir); make package; cd -;)

tar:	clean
	tar cfj ../../ebws_$(VERSION).tbz $(shell find ../../EmbeddedBinaryWebServer/ -type f | grep -vE "/.svn$$|/.svn/" )

