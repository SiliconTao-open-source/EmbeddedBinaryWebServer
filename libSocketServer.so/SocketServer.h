#ifndef included_SocketServer
#define included_SocketServer

#include <unistd.h>
#include <netinet/in.h>

class STString;	
class STHash;
class STList;
class STIntList;
class STXlog;
class STTime;
	
struct stBinaryPacket
{
	int Size;
	char* Data;	
};

class SocketServer
{
	public:
		SocketServer(int SetPort, STHash *_SERVER_Hash, STXlog *NewXlog);
		~SocketServer();	

		/**
	    *	Accepts in coming connections and creates a new socket to deal with the data flow.
		 */
		int Listen();
	
		/**
	    *	Returns data size if no erro or -1 if error.
		 */
		int Receive(char *InTcpBuffer, int BufferSize, int *DataCount);

		/**
		 *	Send data that is stored in text format.
		 */
		void Send(const STString& DataToSend);
	
		void CloseConnection();
	
		/**
		 *	Send data that is stored in binary format.
		 */
		void BinarySend(STList* BinaryPacketList);
	
		/**
	 	 * Returns the current socket.
		 */
		int CurrentSocket();
		
		/**
		 * Turns all open sockets to non blocking in preperation for a shutdown
		 */
		void UnblockSockets();
		
		/**
		 * Change the blocking state of the receiveing socket.
		 */
		void SetBlocking(bool NewBlockState);
	
	private:
		STTime* TimeInt;
		int Port;
		STHash *_SERVER;
		socklen_t SizeOfsockaddress;
		struct sockaddr_in ClientAddress;
		int ListeningSocket, ConnectedSocket;	
		STIntList* SocketStack;
		int OpenListenSocket();
};

#endif // included_SocketServer
