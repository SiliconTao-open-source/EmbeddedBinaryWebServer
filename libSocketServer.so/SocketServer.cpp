#include "SocketServer.h"
#include <STXlog.h>
#include <STTime.h>
#include <STList.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <dlfcn.h>
#include <time.h>
#include <sys/stat.h>
#include <STString.h>
#include <STHash.h>
#include <STIntList.h>
#include <signal.h>

#define BUFFER_SIZE 8096

SocketServer::SocketServer(int SetPort, STHash *_SERVER_Hash, STXlog *NewXlog)
{
	// TimeInt = new STTime();
	xlog = NewXlog;
	// xlog->log("SocketServer::SocketServer(int SetPort, STHash *_SERVER_Hash, STXlog *NewXlog)\n");
	_SERVER = _SERVER_Hash;
	Port = SetPort;
	ListeningSocket = OpenListenSocket();
	SocketStack = new STIntList();
}

SocketServer::~SocketServer()
{
	// delete TimeInt;
	while(SocketStack->Count())
	{
		close(SocketStack->Item(0));
		SocketStack->Delete(0);
	}
	delete SocketStack;
	close(ListeningSocket);
	
}

void SocketServer::BinarySend(STList* BinaryPacketList)
{
	// xlog->log("void SocketServer::BinarySend(STList* BinaryPacketList)\n");
	int BytesSent, MoreSent, LimitAttempts;
	struct stBinaryPacket* BPacket;
	int LastReportedError = 11; // It is okay to get error 11, 'Resource temporarily unavailable' so do not report that one.
	for(int i = 0; i < BinaryPacketList->Count(); i++)
	{
		LimitAttempts = 0;
		BPacket = (struct stBinaryPacket*)BinaryPacketList->Item(i);
		BytesSent = 0;
		if(BPacket->Size > 0)
		{				
			MoreSent = send(ConnectedSocket, BPacket->Data, BPacket->Size, MSG_DONTWAIT|MSG_NOSIGNAL);
			if(MoreSent < 0)
			{
				if(errno != LastReportedError) xlog->log("SocketServer::BinarySend line 61 errno=%d '%s'\n", errno, strerror(errno));
				LastReportedError = errno;
				if(errno == 32) return;
				MoreSent = 0;
				usleep(100);
			}
			BytesSent += MoreSent;
			while((BytesSent != BPacket->Size) && (LimitAttempts < 1000))
			{
				MoreSent = send(ConnectedSocket, BPacket->Data + BytesSent, BPacket->Size - BytesSent, MSG_DONTWAIT|MSG_NOSIGNAL);
				if(MoreSent < 0)
				{
					usleep(1);
					if(errno != LastReportedError) xlog->log("SocketServer::BinarySend line 71 errno=%d '%s'\n", errno, strerror(errno));
					LastReportedError = errno;
					if(errno == 32) return;
					MoreSent = 0;
					usleep(100);
				}
				BytesSent += MoreSent;
				LimitAttempts++;
			}			
		}			
	}
	// xlog->log("void SocketServer::BinarySend is done sending %d bytes of data to socket %d\n", BytesSent, ConnectedSocket);
}
	
void SocketServer::CloseConnection()
{
	if((*_SERVER)["debugsockets"] == "on")
	{
		xlog->log("void SocketServer::CloseConnection() socket shutdown\n");
	}
	// xlog->log("Not closing socket anymore. We added it to the SocketStack so we can close it later\n");
	close(ConnectedSocket);
	
	shutdown(ConnectedSocket, 2);
	if(SocketStack->Count() > 5)
	{
		int KillSocket = SocketStack->Item(0);
		//close(KillSocket);
		SocketStack->Delete(0);
	}

	//0 – Further receives are disallowed.
	//1 – Further sends are disallowed.
	//2 – Further sends and receives are disallowed (like close()).
	// xlog->log("void SocketServer::CloseConnection() ConnectedSocket = 0;\n");
	ConnectedSocket = 0;
}

void SocketServer::UnblockSockets()
{
	fcntl(ListeningSocket, F_SETFL, O_NONBLOCK);
	
	if(ConnectedSocket > 0)
	{
		fcntl(ConnectedSocket, F_SETFL, O_NONBLOCK);
	}	
}

int SocketServer::CurrentSocket()
{
	return(ConnectedSocket);
}

void SocketServer::Send(const STString& DataToSend)
{
	int BytesSent, MoreSent, LimitAttempts;
	int LimitedSend = 8192;
	int LastReportedError = 0;
	int SlowerSocket = 0;
	//xlog->log("Begin SocketServer::Send at %f", TimeInt->Now());
	// xlog->log("void SocketServer::Send(STString DataToSend) ConnectedSocket = %d\n", ConnectedSocket);
	if(ConnectedSocket > 0)
	{		
		BytesSent = 0;
		if(LimitedSend > DataToSend.Len())
		{
			LimitedSend = DataToSend.Len();
		}
		if((*_SERVER)["debugsockets"] == "on")
		{
			xlog->log("send ConnectedSocket = %d, size = %d\n", ConnectedSocket, LimitedSend);
		}
		MoreSent = send(ConnectedSocket, (const char*)DataToSend, LimitedSend, MSG_DONTWAIT|MSG_NOSIGNAL);
		// xlog->log("send ConnectedSocket = %d\n", ConnectedSocket);
		//xlog->log("MoreSent = %d\n", MoreSent);
		if(MoreSent < 0)
		{
			if(errno != LastReportedError) xlog->log("SocketServer::Send line %d errno=%d '%s'\n", __LINE__, errno, strerror(errno));
			LastReportedError = errno;			
			if(errno == 32) return;
			usleep(1); // + SlowerSocket);
			MoreSent = 0;
		}
		BytesSent += MoreSent;
		LimitAttempts = 0;
		// xlog->log("while((BytesSent %d != DataToSend.Len() %d) && (LimitAttempts 0 < 1000))\n", BytesSent, DataToSend.Len());
		while((BytesSent != DataToSend.Len()) && (LimitAttempts < 1000))
		{
			if(LimitedSend > DataToSend.Len() - BytesSent)
			{
				LimitedSend = DataToSend.Len() - BytesSent;
			}			
			// xlog->log("Send BytesSent %d != BufferIndex %d\n", BytesSent, DataToSend.Len());
			// xlog->log("Send LimitedSend = %d\n", LimitedSend);
			MoreSent = send(ConnectedSocket, (const char*)DataToSend + BytesSent, LimitedSend, MSG_DONTWAIT|MSG_NOSIGNAL);
			if(MoreSent < 0)
			{
				if(errno != LastReportedError) xlog->log("SocketServer::Send line %d errno=%d '%s'\n", __LINE__, errno, strerror(errno));
				LastReportedError = errno;
				if(errno == 32) return;
				usleep(1); // + SlowerSocket);
				MoreSent = 0;
			}
			BytesSent += MoreSent;
			LimitAttempts++;
		}			
		/*if(MoreSent == -1)
		{
			CloseConnection();
		}*/
	} else
	{
		xlog->log("Oops! void SocketServer::Send Trying to send to a socket that is closed\n");
	}
	//xlog->log("Done SocketServer::Send at %f", TimeInt->Now());
	// xlog->log("void SocketServer::Send is done sending %d bytes of data to socket %d\n", DataToSend.Len(), ConnectedSocket);
}

int SocketServer::Listen()
{
	//xlog->log("Begin SocketServer::Listen at %f", TimeInt->Now());
	int ReturnErrorCode = 0;
	//char* DontCare = new char[10];
	// This is going to sit and wait for a client to request data.
	SizeOfsockaddress = sizeof(struct sockaddr);
	// SizeOfsockaddress = 0;
	
	if((*_SERVER)["debugsockets"] == "on")
	{
		xlog->log("ConnectedSocket = accept(ListeningSocket, (struct sockaddr*)&ClientAddress, &SizeOfsockaddress); begin line %d at %f", __LINE__, TimeInt->Now());
	}
	ConnectedSocket = accept(ListeningSocket, (struct sockaddr*)&ClientAddress, &SizeOfsockaddress);
	if((*_SERVER)["debugsockets"] == "on")
	{
		xlog->log("done accept at %f", TimeInt->Now());
		xlog->log("ListeningSocket = %d ConnectedSocket = %d\n", ListeningSocket, ConnectedSocket);
	}
	
	SocketStack->Add(ConnectedSocket);
	if(ConnectedSocket > 0)
	{
		// We have our listen socket in blocking mode and then we do not block the recieving socket.
		// Maybe we should block the receiving socket here.
		// fcntl(ConnectedSocket, F_SETFL, O_NONBLOCK);
		ReturnErrorCode = 1;
		(*_SERVER)["REMOTE_ADDR"] = inet_ntoa(ClientAddress.sin_addr);
		
		// fprintf(stderr, "int SocketServer::%s line %d ClientAddress.sin_addr '%s'\n", __FUNCTION__, __LINE__, inet_ntoa(ClientAddress.sin_addr));
		// fprintf(stderr, "int SocketServer::%s line %d (*_SERVER)['REMOTE_ADDR']='%s'\n", __FUNCTION__, __LINE__, (const char*)(*_SERVER)["REMOTE_ADDR"]);

		/*if((*_SERVER)["debugsockets"] == "on")
		{
			xlog->log("begin recv at %f", TimeInt->Now());
		}		
		int ErrCode = recv(ConnectedSocket, DontCare, 9, MSG_WAITALL | MSG_PEEK);
		if((*_SERVER)["debugsockets"] == "on")
		{
			xlog->log("done recv at %f", TimeInt->Now());
		}		
		if(ErrCode < 0)
		{
			xlog->log("SocketServer::Listen line %d %d '%s'\n", __LINE__, errno, strerror(errno));
		}*/
	} else
	{
		ReturnErrorCode = -1;
		xlog->log("int SocketServer::Listen() <<<<<<<<<<<<<<<<<<<< CloseConnection(); >>>>>>>>>>>>>>>>>>>>>>\n");
		// Cannot close a socket that is not open
		//CloseConnection();
	}		
	//delete[] DontCare;
	if((*_SERVER)["debugsockets"] == "on")
	{
		xlog->log("done SocketServer::Listen at %f", TimeInt->Now());
	}		
	
	return(ReturnErrorCode);
}

int SocketServer::Receive(char *InTcpBuffer, int BufferSize, int *DataCount)
{
	int NewDataCount = 0;
	if((*_SERVER)["debugsockets"] == "on")
	{
		xlog->log("begin read loop line %d at %f", __LINE__, TimeInt->Now());
	}		
	
	#warning ======================================================================================================================================================
	#warning 
	#warning set this so the socket times out. client may die in the middle of a connection and lock the system.	
	#warning SO_RCVTIMEO
	#warning 
	#warning ======================================================================================================================================================
	
	
	// If we are blocking then maybe we can read again if the buffer is full?
	NewDataCount = read(ConnectedSocket, InTcpBuffer, BufferSize);
	//xlog->log("NewDataCount = %d\n", NewDataCount);
	if(NewDataCount > 0)
	{
		*DataCount = NewDataCount;
	} else	
	{
		return(-1);
	}
	return(*DataCount > 0);
}

void SocketServer::SetBlocking(bool NewBlockState)
{
	int opts = fcntl(ConnectedSocket, F_GETFL);

	if(NewBlockState == true)
	{
		opts &= ~O_NONBLOCK;
	} else
	{
		opts |= O_NONBLOCK;
	}
	fcntl(ConnectedSocket, F_SETFL, opts);
}

int SocketServer::OpenListenSocket()
{
	//xlog->log("int SocketServer::OpenListenSocket()\n");
   int sockfd;
   struct sockaddr_in my_addr;	// my address information

   if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
   {
      perror("socket");
      exit(1);
   }
	
	int on = 1;
	// This function returns an int but I don't know what if any meaning it could have.
	setsockopt( sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) );
	
	my_addr.sin_family = AF_INET;
   my_addr.sin_port = htons(Port);
   my_addr.sin_addr.s_addr = INADDR_ANY;
   memset(&(my_addr.sin_zero),'\0',8);
   
   if(bind(sockfd,(struct sockaddr *)&my_addr,sizeof(struct sockaddr)) == -1)
   {
      // perror("bind");
      // exit(1);
   }

   if(listen(sockfd,1) == -1)
   {
      // perror("listen");
		xlog->log("Cannot open socket on that port, is it being used by another program? SocketServer::OpenListenSocket() line %d errno %d '%s'\n", __LINE__, errno, strerror(errno));
      exit(1);
   }
	//xlog->log("int SocketServer::OpenListenSocket() ConnectedSocket = 0;\n");
	ConnectedSocket = 0;
	
   return(sockfd); 
}
