#ifndef included_PageObject
#define included_PageObject

#include "WebObjects.h"

class PageObject: public WebObjects
{
	public:		
		int Main();	
		int Close();
};

#endif // included_PageObject
