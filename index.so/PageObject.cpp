#include "PageObject.h"
#include "PageServer.h"
#include <STHash.h>
#include <STString.h>

int PageObject::Main()
{	
	Post("<P>Welcome to EBWS Embedded Binary Web Server</P>");
	
	Post("<BR><BR><B><U>_SERVER</U></B><BR>");
	Post("<A HREF=\"/About_Ebws.html\">Read more about EBWS here</A><BR><BR>\n");
	for(int i = 0; i < _SERVER->Count(); i++)
	{
		Post("_SERVER['" + _SERVER->Key(i) + "'] = '" + (*_SERVER)[_SERVER->Key(i)] + "'<BR>\n");
	}
	
	// You can store a persistant variable for an object or library that will stay active until Close is called.
	STHash* ExampleVar;
	ExampleVar = (STHash*)(*_GLOBAL)["MY_HASH_VAR"].ToInt();
	if(ExampleVar == 0)
	{
		STString IntToStr;
		ExampleVar = new STHash();
		(*ExampleVar)["DUDE"] = "You got data";
		(*_GLOBAL)["MY_HASH_VAR"] = IntToStr.Sprintf("%d", (unsigned int)ExampleVar);
	}	
	return(0);
}

int PageObject::Close()
{
	Log("Closing index.so and deleting ExampleVar.\n");
	STHash* ExampleVar;
	ExampleVar = (STHash*)(*_GLOBAL)["MY_HASH_VAR"].ToInt();
	if(ExampleVar != 0)
	{
		delete ExampleVar;
	}	
}
