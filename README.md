# EmbeddedBinaryWebServer

This was a project from 2006 to create a web server to run in a PC104 with a very small amount of RAM and disk space.

This project requires the [ST Libs](https://gitlab.com/SiliconTao-open-source/libsilicontao)

The main reason for this project was to create a very small and fast web server that could serve single requests.

Pages can be served as text files (HTML, CSS, JS, TXT) or compiled C++ objects. 
