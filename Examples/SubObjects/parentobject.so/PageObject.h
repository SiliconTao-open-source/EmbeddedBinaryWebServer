#ifndef included_PageObject
#define included_PageObject

#include "WebObjects.h"
#include <STString.h>

class ClientDB;
	
class PageObject: public WebObjects
{
	private:
		STString ReturnString;
		ClientDB* Db;

		void GoLogin();
		void SetSessionId(int UserId);
		void GoLoadContent();	
		bool InitDatabase();	
		void PostDefaultPage();
		void ErrorWebPage();
		STString CleanStrAlphaNumb(const STString& SubmitStr);
	
	public:		
		int Main();	
};

#endif // included_PageObject
