#include "PageObject.h"
#include "PageServer.h"
#include <STHash.h>
#include <dlfcn.h>

int PageObject::Main()
{	
	Post("This is the main page<BR>\n");
	
	STString ContentLibraryName = (*_SERVER)["DOCUMENT_ROOT"] + (*_SERVER)["SCRIPT_DIR"] + "subobject.so";
	Post("ContentLibraryName = '"+ContentLibraryName+"'<BR>\n");
	
	void* LibPointer = dlopen((const char*)ContentLibraryName, RTLD_LAZY);	
	int (*ObjectEntryFP)(void *, void *);		// FP is Function Pointer
	if(LibPointer)
	{
		Post("Lib loaded<BR>\n");
		void* GenericPointer;
		int (*InitializeFP)();		// FP is Function Pointer
		GenericPointer = dlsym(LibPointer, "Initialize");
		if(GenericPointer)
		{
			Post("Lib Initialize located<BR>\n");
			InitializeFP = (int (*)())GenericPointer;
			InitializeFP();
		} else
		{
			Post("Lib Initialize not located<BR>\n");
		}
		GenericPointer = dlsym(LibPointer, "ObjectEntry");
		if(GenericPointer)
		{
			Post("Lib ObjectEntry located<BR>\n");
			ObjectEntryFP = (int (*)(void *, void *))GenericPointer;
			ObjectEntryFP(PageContent, GlobalVars);
		} else
		{
			Post("Lib ObjectEntry not located<BR>\n");
		}
		dlclose(LibPointer);
	} else
	{
		Post("Lib not loaded<BR>\n");
	}
	return(0);
}
