#include "PageObject.h"
#include "PageServer.h"
#include <STHash.h>
#include <STString.h>

int PageObject::Main()
{	
	// If you want to make it so that this library cannot be accessed directly do something like this.
	if((*_SERVER)["SCRIPT_NAME"] == "/parentobject.so")
	{
		Post("This is a good page to see here.");
	} else
	{
		Post("You are not able to view this page. Go back to the index.");
	}
	return(0);
}
