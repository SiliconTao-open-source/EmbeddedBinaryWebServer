#ifndef included_PageObject
#define included_PageObject

#include "WebObjects.h"
#include <STString.h>

class ClientDB;
	
class PageObject: public WebObjects
{
	public:		
		int Main();	
};

#endif // included_PageObject
