function downloadBySelection(selectedItem, targetItem)
{
	var selectElement = document.getElementById? document.getElementById(selectedItem) : ""
	if (selectElement != "" && selectElement.options[selectElement.selectedIndex].value != "")
	{
		ajaxDownload(selectElement.options[selectElement.selectedIndex].value, targetItem)
	}
}

function ajaxDownload(urlPage, targetItem)
{
	var getHTTP = false
	if (window.XMLHttpRequest)
	{
		getHTTP = new XMLHttpRequest()
	} else 
	{
		alert("Your browser is to old and does not support Ajax\nPlease visit http://GetFireFox.com")
		return false
	} 
	
	// Set the call back on page load event.
	getHTTP.onreadystatechange=function()
	{
		loadPage(getHTTP, targetItem)
	}
	getHTTP.open('GET', urlPage, true)
	getHTTP.send(null)
}

function loadPage(getHTTP, targetItem)
{
	if (getHTTP.readyState == 4 && (getHTTP.status == 200 || window.location.href.indexOf("http") == -1))
	{
		document.getElementById(targetItem).innerHTML = getHTTP.responseText
	}
}
