#include "PageObject.h"
#include "PageServer.h"
#include <STHash.h>

int PageObject::Main()
{	
	STHash MyOwnHash;
	
	// You use Endorse to make the hash variable useable from the file you call with EvalFile.
	Endorse(&MyOwnHash, "MyOwnHash");
	
	// This value will now be inserted for the variable in the HTML file.
	MyOwnHash["field1"] = "on";
	
	if((*_GET)["showcode"] == "")
	{
		EvalFile((*_SERVER)["SCRIPT_DIR"] + "eval.html");
		Post("<BR><BR><B><U>View the code</U></B><BR>");
		Post("<A HREF='" + (*_SERVER)["SCRIPT_NAME"] + "?showcode=PageObject.cpp'>PageObject.cpp</A><BR>");
		Post("<A HREF='" + (*_SERVER)["SCRIPT_NAME"] + "?showcode=eval.html'>eval.html</A><BR>");
	} else
	{
		Post("<A HREF='" + (*_SERVER)["SCRIPT_NAME"] + "'>Back</A><BR>\n");
		PostFromFile((*_SERVER)["SCRIPT_DIR"] + "starttable.html");
		PostFromFilePre((*_SERVER)["SCRIPT_DIR"] + (*_GET)["showcode"]);
		PostFromFile((*_SERVER)["SCRIPT_DIR"] + "endtable.html");
	}		
	return(0);
}
