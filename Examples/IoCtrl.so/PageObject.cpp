#include "PageObject.h"
#include "PageServer.h"
#include <STHash.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

void PageObject::SetState(int NewState)
{
	CurrentState = NewState;
	#ifdef  __arm__
	if(NewState)
	{
		*PBDR = 0xFF;
	} else
	{
		*PBDR = 0x00;		
	}
	#endif		
}

int PageObject::Main()
{
	#ifdef  __arm__
   fd = open("/dev/mem", O_RDWR);
   start = (unsigned char*) mmap(0, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x80840000);
	
   PBDR = (unsigned int *)(start + 0x04);     // port b
   PBDDR = (unsigned int *)(start + 0x14);    // port b direction register
   PEDR = (unsigned int *)(start + 0x20);     // port e data
   PEDDR = (unsigned int *)(start + 0x24);    // port e direction register
   GPIOBDB = (unsigned int *)(start + 0xC4);  // debounce on port b

   *PBDDR = 0xFF;			      // Output
   *PEDDR = 0xff;                             // all output (just 2 bits)
   *GPIOBDB = 0x01;			      // enable debounce on bit 0
		
	// Pre set the state. We cannot use SetState here because we do not have access to web objects yet.
	*PBDR = 0x00;		
   #endif
	
	CurrentState = 0;

	STHash LedState;
	
	if((*_GET)["statechange"] == "y")
	{
		if((*_GET)["ctrlname"] == "greenled")
		{
			if((*_GET)["newstate"] == "on")
			{
				SetState(1);
			} else
			{
				SetState(0);				
			}
		}
	} else
	{
		LedState["Green"] = "CHECKED";
		Endorse(&LedState, "LedState");
		
		#ifdef  __arm__
		EvalFile("/Examples/IoCtrl/IoCtrl.html");
		HPost("<SCRIPT LANGUAGE=\"JavaScript\" TYPE=\"text/javascript\" SRC=\"/Examples/IoCtrl/IoCtrl.js\"></SCRIPT>");
		#else
		Post("<P><B><BIG><U>This is not an ARM processor. IO Controls are dissabled.</U></BIG></B></P>");
		#endif
		PageContent->ListDirectory("/");
		Post("<BR><BR><BR><BR>");
		if((*_POST)["showvars"] == "Y")
		{
			ShowVars();
		}
	}

	#ifdef  __arm__
	close(fd);		
	#endif
	
	return(0);

}
