#ifndef included_PageObject
#define included_PageObject

#include "WebObjects.h"

class PageObject: public WebObjects
{
	public:		
		int fd;
		volatile unsigned int *PEDR, *PEDDR, *PBDR, *PBDDR, *GPIOBDB;
		int i;
		unsigned char state;
		unsigned char *start;
		int CurrentState;
	
		void SetState(int NewState);
		int Main();	
};

#endif // included_PageObject
