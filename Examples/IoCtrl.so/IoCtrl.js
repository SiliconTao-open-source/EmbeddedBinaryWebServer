function SetState(LedCtrl)
{
	var getHTTP = false
	if (window.XMLHttpRequest)
	{
		getHTTP = new XMLHttpRequest()
	} else 
	{
		alert("Your browser is to old and does not support Ajax\nPlease visit http://GetFireFox.com")
		return false
	} 
	
	var selectElement = document.getElementById(LedCtrl)
	var NewState = "off"
	if (selectElement.checked) 
	{
		NewState = "on"
	}	
	
	getHTTP.open('GET', "/index.so?statechange=y&ctrlname=" + LedCtrl + "&newstate=" + NewState, true)
	getHTTP.send(null)
}
