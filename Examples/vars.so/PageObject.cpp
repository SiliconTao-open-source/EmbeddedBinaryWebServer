#include "PageObject.h"
#include "PageServer.h"
#include <STHash.h>

int PageObject::Main()
{	
	int i;
	Post("<P ALIGN='CENTER'><BIG><B><I>Server variables</I></B></BIG></P>");
	
	Post("<BR><BR><B><U>_SERVER</U></B><BR>");
	for(i = 0; i < _SERVER->Count(); i++)
	{
		Post("_SERVER['" + _SERVER->Key(i) + "'] = '" + (*_SERVER)[_SERVER->Key(i)] + "'<BR>\n");
	}

	Post("<BR><BR><B><U>_POST</U></B><BR>");
	for(i = 0; i < _POST->Count(); i++)
	{
		Post("_POST['" + _POST->Key(i) + "'] = '" + (*_POST)[_POST->Key(i)] + "'<BR>\n");
	}
	
	Post("<BR><BR><B><U>_GET</U></B><BR>");
	for(i = 0; i < _GET->Count(); i++)
	{
		Post("_GET['" + _GET->Key(i) + "'] = '" + (*_GET)[_GET->Key(i)] + "'<BR>\n");
	}
	return(0);
}
