#include "WebServer.h"
#include "DebugBits.h"
#include <STString.h>
#include <STSystemAPI.h>
#include <STDriveOps.h>
#include <STXlog.h>

// Used by fork
#include <sys/types.h>
#include <unistd.h>

// Used by signel
#include <signal.h>

//Used by umask
#include <sys/types.h>
#include <sys/stat.h>

// Used by freopen
#include <stdio.h>

// Used by exit
#include <stdlib.h>

// Used by fopen
#include <stdio.h>

STXlog* xlog;
void ShutDown(int SigNum);
void ShowHelp(int ExitError);
WebServer *Server1;

int main(int argc, char **argv)
{
   int TempReturn;    
	unsigned int DebugSettings = 0;
	bool OptionSet;
	int Pid = 0;
	int DefaultPort = 80;
	int Port = 0;
	STString DocRoot, HostName, ParseArg, ServerArch;
	
	/*
	// This is to test the new QuickStep feature in STString for faster finding positions on bountry blocks.
	//         0    5    0    5    0    5    0    5    0    5    0    5    0    5    0    5    0 	
	DocRoot = "abcdefghijklmnopqrstuv---wx--yz1234567890 ----------ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int WTF0 = DocRoot.Pos("------ABCD", -1);	
	int WTF1 = DocRoot.Pos("------ABCD", 5);	
	int WTF2 = DocRoot.Pos("------ABCD");
	DocRoot = "------ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int WTF3 = DocRoot.Pos("------ABCD");	
	DocRoot = "abcdefghijklmnopqrstuv---wx--yz1234567890 ----------ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int WTF4 = DocRoot.Pos("------1234");	
	DocRoot = "------ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int WTF5 = DocRoot.Pos("------1234");	
	DocRoot = "hZ";
	int WTF6 = DocRoot.Pos("------1234");	
	printf("WTF0 = %d, WTF1 = %d, WTF2 = %d, WTF3 = %d, WTF4 = %d, WTF5 = %d, WTF1 = %d\n", WTF0, WTF1, WTF2, WTF3, WTF4, WTF5, WTF6); 
	
	// */	
	
	
	STSystemAPI* SystemAPI = new STSystemAPI();
	int UserID = SystemAPI->GetUserID();
	
	#ifdef __arm__
	ServerArch = "ARM";
	#else
	ServerArch = "i386";
	#endif

	DocRoot = "";
	if(UserID == 0) DocRoot = "/var/www";
	HostName = "http://www.SiliconTao.com";
	
	int i;
	for (i = 1; i < argc; i++)
	{
		ParseArg = argv[i];
		OptionSet = false;
		if((ParseArg == "-d") || (ParseArg == "-debug") || (ParseArg == "--debug"))
		{
			DebugSettings |= DEBUG_GENERAL;
			OptionSet = true;
		}		
		if(ParseArg == "-dx")
		{
			DebugSettings |= DEBUG_GENERAL | DEBUG_LINK;
			OptionSet = true;
		}
		if(ParseArg == "-dr")
		{
			DebugSettings |= DEBUG_GENERAL | DEBUG_REQUESTS;
			OptionSet = true;
		}
		if(ParseArg == "-dp")
		{
			DebugSettings |= DEBUG_GENERAL | DEBUG_POSTS;
			OptionSet = true;
		}
		if(ParseArg == "-ds")
		{
			DebugSettings |= DEBUG_GENERAL | DEBUG_SOCKETS;
			OptionSet = true;
		}
		if((ParseArg == "-h") || (ParseArg == "-help"))
		{
			OptionSet = true;
			ShowHelp(0);
		}
		if(ParseArg == "-port")
		{
			OptionSet = true;
			if( argc > i)
			{
				i++; 
				ParseArg = argv[i];
				Port = ParseArg.ToInt();
				DefaultPort = Port;
				if(Port < 1)
				{
					printf("Error: Invalid port\n");
					ShowHelp(1);
				} else
				if((Port != 80) && (Port <= 1024))
				{
					printf("Warning: Using non standard reserved port may conflict with other services\n");
				} else
				{
					printf("Info: Setting port to %d\n", Port);
				}
			} else
			{
				printf("Error: You must set a port number to use the -port option\n");
				ShowHelp(1);
			}
		}
		if(ParseArg == "-docroot")
		{
			OptionSet = true;
			if( argc > i)
			{
				i++; 
				DocRoot = argv[i];
			} else
			{
				printf("Error: You must set a path to use the -docroot option\n");
				ShowHelp(1);
			}
		}
		if(ParseArg == "-host") 
		{
			OptionSet = true;
			if( argc > i)
			{
				i++; 
				HostName = argv[i];
			} else
			{
				printf("Error: You must set a domain to use the -host option\n");
				ShowHelp(1);
			}
		}
		if((UserID != 0) && (OptionSet == false))
		{
			OptionSet = true;
			STDriveOps Do;
			printf("Look at dir %s\n", argv[i]);
			if(Do.DirExists(argv[i]))
			{	
				DocRoot = argv[i];
				printf("DocRoot = %s\n", (const char*)DocRoot);
				DefaultPort = 8080;
			}
		}
		if(OptionSet == false)
		{
			printf("Error: Unknown option '%s'\n", argv[i]);
			ShowHelp(1);
		}
	}
	
	if(Port == 0) Port = DefaultPort;
		
	xlog = new STXlog(DebugSettings & DEBUG_GENERAL);
	
	// Call back to let the object safely shutdown.
	signal(SIGTERM, ShutDown);
	
	if(UserID == 0)
	{
		if(! DebugSettings & DEBUG_GENERAL)
		{
			int ChildPid = 0;
			ChildPid = fork();
			// Seperate from the controling terminal.
			if(ChildPid)
			{
				// This is the parent of the fork
				exit(0);
			}
			
			// Become session leader and detach from parent application.
			chdir("/");
			setsid();
			umask(0);
			
			// Some people advise closing STDIO files, others say that they should be redirected to /dev/null to prevent IO crashes.
			//	Not sure what is better.
			freopen("/dev/null", "w", stdout);
			freopen("/dev/null", "w", stderr);
			freopen("/dev/null", "r", stdin);
			
			ChildPid = 0;
			ChildPid = fork();
			// Second fork prevents accidentaly gaining a controlling terminal.
			if(ChildPid)
			{
				exit(0);
			}
		} else
		{
			printf("--------------------------------------------------------------\n");
			printf("Embedded Binary Web Server for %s serving %s\n", (const char*)ServerArch, (const char*)DocRoot);
		}
		
		int AppPid = SystemAPI->GetAppPid();
		STString AppName = SystemAPI->GetAppNameFromPid(AppPid);
		STString PidLock;
		PidLock.Sprintf("/var/run/%s.pid", (const char*)AppName);
		FILE* FilePointer = fopen((const char*)PidLock, "w");
		if(FilePointer)
		{
			fprintf(FilePointer, "%d", AppPid);
			fclose(FilePointer);
		}
		xlog->log("%s running at PID %d", (const char*)AppName, AppPid);
	} else
	{
		if(DocRoot == "")
		{
			DocRoot = SystemAPI->GetUserHome(UserID);
		}
		HostName = SystemAPI->GetUserName(UserID);
		printf("Serving '%s' at port %d\n", (const char*)DocRoot, Port);
	}	
	delete SystemAPI;
	fflush(NULL);
	// Create a listening master socket at port 80
	Server1 = new WebServer(HostName, DocRoot, Port, DebugSettings, ServerArch, xlog);
	delete(Server1);
	
   return(TempReturn);
}

void ShutDown(int SigNum)
{
	Server1->ShutDown();
	xlog->log("Shutdown");
}

void ShowHelp(int ExitError)
{
	printf("ebws [-port <PORT NUMBER [80] >] [-docroot <PATH TO DOCUMENT ROOT [/var/www] >] [-host <DOMAIN NAME [localhost] >] [-d|-debug] | [-h|--help|-help]\n");
	printf("Debugging options\n -d\tGeneral debuging, do not fork to daemon, print messages to stdio\n");
	printf(" -dx\tGeneral debug plus show details about dynamic link messages\n");
	printf(" -dr\tGeneral debug plus show HTTP requests\n");
	printf(" -dp\tGeneral debug plus show response postings to HTTP requests\n");
	printf(" -ds\tGeneral debug plus show socket information\n");
	exit(ExitError);
}
