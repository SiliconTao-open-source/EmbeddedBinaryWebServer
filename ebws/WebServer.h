#ifndef included_WebServer
#define included_WebServer

#include <unistd.h>
#include <netinet/in.h>
#include <STString.h>

class SocketServer;
class PageServer;
class STStringList;
class STHash;
class STList;
class STDynamicDataLink;
class STXlog;
class STTime;

class WebServer
{
	public:
		bool KeepRunning;
		WebServer(const char* HostName, const char* NewRoot, int Port, unsigned int DebugSettings, const char* ServerArch, STXlog *NewXlog);
		~WebServer();	
		int OpenListenSocket(int TcpPort);
		void LookForConnect();
		int Receive();
		void CloseConnection();
		void ParseRequest();
		void Send(const STString& DataToSend);
		void ProcessURI(const STString& CommandString);
		void ProcessGetPostData(const STString& ContentString, STHash *GetOrPost);
		void ParseCookies(const STString& ContentString);
		const char* UrlDecode(const STString& OrigStr);
		void ListDirectory();
		void DumpBinaryFile();
		void ShutDown();
		void ClearRxBufferList();
		STString GetData(const char* DataField);
		void ProcessFileUpload(const STString& BoundaryString);
		struct stBinaryPacket* ShiftOutBuffer(int *SamplePosition, struct stBinaryPacket* BPacket);
		struct stBinaryPacket* EatLine(int *SamplePosition, struct stBinaryPacket* BPacket);
		struct stBinaryPacket* EatReturn(int *SamplePosition, struct stBinaryPacket* BPacket);

		// Do not call the old way
		// void Old_ProcessFileUpload(const STString& BoundaryString);
	
	
		void ParseInfoOnUploadedFile(STStringList* UploadInfo);
		void DataSplit(const STString& SpitLine, STHash* FillHash);
		void Split(const STString& StringToSpit, const char* SplitMarker, STStringList* FillParts);
	
		SocketServer *Socket;
		int RequestCounter;
		STList *RxBufferList;
		STHash *GlobalVars, *_SERVER, *_GET, *_POST, *_COOKIE, *_GLOBAL, *_FILE;
		STStringList* Request;
		PageServer* PageContent;
		STString ReturnString;
		bool ClientBrowserSendingCRLF;
	
	private:
		STXlog *xlog;
		STTime* TimeInt;
};

#endif // included_WebServer
