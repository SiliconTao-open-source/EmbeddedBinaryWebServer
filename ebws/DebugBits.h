#ifndef included_DebugBits
#define included_DebugBits

#define DEBUG_GENERAL 	0x00000001
#define DEBUG_LINK		0x00000002
#define DEBUG_POSTS		0x00000004
#define DEBUG_REQUESTS	0x00000008
#define DEBUG_SOCKETS	0x00000010

#endif // DebugBits.h
