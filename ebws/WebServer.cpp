#include "ModuleControl.h"
#include "WebServer.h"
#include <STXlog.h>
#include "DebugBits.h"
#include <PageServer.h>
#include <SocketServer.h>
#include <stdlib.h>
#include <stdio.h>
#include <STHash.h>
#include <STList.h>
#include <STTime.h>
#include <STParserOps.h>
#include <STDriveOps.h>
#include <STStringList.h>
#include <STDynamicDataLink.h>
#include <STStringsEx.h>
#include <STSystemAPI.h>
#include <STTime.h>

// Remove this line, it is only for a debug I did
#include <string.h>

// Set the buffer small so I can test the read control to make sure that data is not lost
//#warning Change the buffer size
#define BUFFER_LIMIT 1024

//#define BUFFER_LIMIT 8192

WebServer::WebServer(const char* HostName, const char* NewRoot, int Port, unsigned int DebugSettings, const char* ServerArch, STXlog *NewXlog)
{
	STTime SpeedTimer;
	xlog = NewXlog;
	KeepRunning = true;
	STString IntToStr;
	TimeInt = new STTime();

	Request = new STStringList();
	_SERVER = new STHash();
	RxBufferList = new STList();
	(*_SERVER)["SERVER_SOFTWARE"] = "Embebedded Binary Web Server Version 1.7.58 ";
	(*_SERVER)["SERVER_SOFTWARE"] += ServerArch;
	(*_SERVER)["DOCUMENT_ROOT"] = NewRoot;
	(*_SERVER)["HOST"] = HostName;
	(*_SERVER)["PORT"] = IntToStr.Sprintf("%d", Port);
	_GET = new STHash();
	_POST = new STHash();
	_COOKIE = new STHash();
	_GLOBAL = new STHash();
	(*_GLOBAL)["XLOG_POINTER"] = IntToStr.Sprintf("%d", (unsigned int)NewXlog);

	// Set debug settings
	(*_GLOBAL)["debug"] = "off";
	(*_GLOBAL)["debuglink"] = "off";
	(*_SERVER)["debugpost"] = "off";
	(*_SERVER)["debugrequest"] = "off";
	if(DebugSettings & DEBUG_GENERAL)
	{
		xlog->log("DEBUG_GENERAL is on\n");
		(*_GLOBAL)["debug"] = "on";
		(*_SERVER)["debug"] = "on";
	}
	if(DebugSettings & DEBUG_LINK)
	{
		xlog->log("DEBUG_LINK is on\n");
		(*_GLOBAL)["debuglink"] = "on";
	}
	if(DebugSettings & DEBUG_POSTS)
	{
		xlog->log("DEBUG_POSTS is on\n");
		(*_SERVER)["debugheader"] = "on";
		(*_SERVER)["debugpost"] = "on";
	}
	if(DebugSettings & DEBUG_REQUESTS)
	{
		xlog->log("DEBUG_REQUESTS is on\n");
		(*_SERVER)["debugrequest"] = "on";
	}
	if(DebugSettings & DEBUG_SOCKETS)
	{
		xlog->log("DEBUG_SOCKETS is on\n");
		(*_SERVER)["debugsockets"] = "on";
	}

	// Memory buffer to store file uploads
	_FILE = new STHash();
	// (*_FILE)["MAX_FILE_SIZE"] = "8192";
	(*_FILE)["MAX_FILE_SIZE"] = IntToStr.Sprintf("%d", 1024 * 1024); // Max size is one mega byte
	(*_FILE)["BUFFER_LIST_POINTER"] = IntToStr.Sprintf("%d", RxBufferList);

	//Initialize();

	Socket = new SocketServer(Port, _SERVER, xlog);

	GlobalVars = new STHash();
	RequestCounter = 0;
	PageContent = new PageServer(Socket, GlobalVars, xlog);
	PageContent->Endorse(_SERVER, "_SERVER");
	PageContent->Endorse(_GET, "_GET");
	PageContent->Endorse(_POST, "_POST");
	PageContent->Endorse(_COOKIE, "_COOKIE");
	PageContent->Endorse(_GLOBAL, "_GLOBAL");
	PageContent->Endorse(_FILE, "_FILE");

	STDynamicDataLink* Link = PageContent->GetDataLink();

	PageContent->Reset();		// Prepare the page body buffer.
	while(KeepRunning)
	{
		if(Receive())	// Get a request for a page.
		{
			SpeedTimer.ResetTimeMarker();
			//if((*_SERVER)["debugrequest"] == "on") xlog->log("ParseRequest();				// Hand parsing over to a page server library.");
			ParseRequest();				// Hand parsing over to a page server library.
			//if((*_SERVER)["debugrequest"] == "on") xlog->log("PageContent->PostPage();	// Post the data that is sent by the serving page.");
			PageContent->PostPage();	// Post the data that is sent by the serving page.
			//if((*_SERVER)["debugrequest"] == "on") xlog->log("PageContent->Reset();		// Prepare the next page body buffer.");
			PageContent->Reset();		// Prepare the next page body buffer.
			if((*_SERVER)["debugrequest"] == "on")
			{
				xlog->log("-------------------------- END REQUEST %d socket %d --------------------------", RequestCounter, Socket->CurrentSocket());
			}
			usleep(1);
			// if((*_GLOBAL)["debug"] == "on") SpeedTimer.PrintTimeMarker();
			//fprintf(stderr, "WebServer done posting event at %f\n", TimeInt->Now());
		} else
		{
			xlog->log("WebServer::WebServer socket died!\n");
		}
		Link->MessageCount();
		//fprintf(stderr, "WebServer done Link->MessageCount(); event at %f\n", TimeInt->Now());
		Socket->CloseConnection();
		//fprintf(stderr, "WebServer done Socket->CloseConnection(); event at %f\n", TimeInt->Now());
	}
	delete Socket;
}

WebServer::~WebServer()
{
	delete _SERVER;
	delete _POST;
	delete _GET;
	delete _COOKIE;
	delete GlobalVars;
	delete Request;
	ClearRxBufferList();
	delete RxBufferList;
	delete TimeInt;
}

void WebServer::ClearRxBufferList()
{
	struct stBinaryPacket* BPacket;
	while(RxBufferList->Count() > 0)
	{
		BPacket = (struct stBinaryPacket*)RxBufferList->Item(0);
		delete[] BPacket->Data;
		delete BPacket;
		RxBufferList->Delete(0);
	}
}

void WebServer::ShutDown()
{
	KeepRunning = false;
	Socket->UnblockSockets();
}

void WebServer::ParseRequest()
{
	if((*_GLOBAL)["debug"] == "on") xlog->log("From %s Request: %s\n", (const char*)((*_SERVER)["REMOTE_ADDR"]), (const char*)((*_SERVER)["REQUEST_URI"]));

	//xlog->log("void WebServer::ParseRequest() PageContent->FileType() = %d\n", PageContent->FileType());
	switch(PageContent->FileType())
	{
		//case FT_UNKNOWN_TEXT:	PageContent->PostFromFilePre((*_SERVER)["REQUEST_URI"]);	break;
		// Direct requests for TEXT files should not be wrapped in a PRE tag.
		case FT_UNKNOWN_TEXT:	PageContent->PostFromFile((*_SERVER)["REQUEST_URI"]);		break;
		case FT_INCLUDE_SCRIPT:	PageContent->IPostFromFile((*_SERVER)["SCRIPT_NAME"]);	break;
		case FT_TEXT_HTML:		PageContent->PostFromFile((*_SERVER)["SCRIPT_NAME"]);		break;
		case FT_DIR_LISTING:		ListDirectory();														break;
		case FT_GENERATED:		PageContent->ExecuteLibrary((*_SERVER)["SCRIPT_NAME"]);	break;
		case FT_BINARY:
		case FT_GRAPHIC:			PageContent->PostBinaryFile((*_SERVER)["REQUEST_URI"]);	break;
		case FT_INTERNAL:			PageContent->ShowVars();											break;
	}
}

void WebServer::DumpBinaryFile()
{
	//xlog->log("void WebServer::DumpBinaryFile()\n");
	PageContent->PostBinaryFile((*_SERVER)["DOCUMENT_ROOT"] + (*_SERVER)["REQUEST_URI"]);
}

void WebServer::ParseCookies(const STString& ContentString)
{
	STString ParsingString, Key, Value;

	STStringList PartsList;

	//xlog->log("void WebServer::ParseCookies()\n");
	Split(ContentString, " ", &PartsList);
	ParsingString = PartsList[0];
	PartsList.Delete(0);
	ParsingString.Remove(0, ParsingString.Pos(" ") + 1);
	if(ParsingString.Len())
	{
		PartsList.Add(ParsingString);
	}
	while(PartsList.Count())
	{
		ParsingString = PartsList[0];
		PartsList.Delete(0);
		//xlog->log("ParsingString.Pos(';', -1) = %d,  ParsingString.Len() = %d\n", ParsingString.Pos(';', -1),  ParsingString.Len());
		if(ParsingString.Pos(";", -1) == ParsingString.Len() - 1)
		{
			ParsingString.Remove(ParsingString.Len() - 1, ParsingString.Len());
			//xlog->log("ParsingString = '%s'\n", (const char*)ParsingString);
		}
		if(ParsingString.Pos("=") > -1)
		{
			Key = ParsingString.Left(ParsingString.Pos("="));
			ParsingString.Remove(0, ParsingString.Pos("=") + 1);
			Value = ParsingString;
			//xlog->log("  - Key = '%s'\n", (const char*)Key);
			//xlog->log("  - Value = '%s'\n", (const char*)Value);
			(*_COOKIE)[Key] = Value;
		}
	}
}

void WebServer::ProcessGetPostData(const STString& ContentString, STHash* GetOrPost)
{
	STString ParsingString, Marker, Key, Value, WorkStr;
	STParserOps ParserOps;
	WorkStr = ContentString;

	if(GetOrPost == _GET)
	{
		ParsingString = WorkStr.Remove(0, WorkStr.Pos("?") + 1);
		WorkStr = ParsingString.Remove(ParsingString.Pos(' '), ParsingString.Len());
	}
	while(WorkStr.Len() > 0)
	{
		Marker = "&";
		ParsingString = ParserOps.ExtractBlock(WorkStr, 0, Marker);
		WorkStr.Remove(0, ParsingString.Len() + 1);
		Marker = "=";
		Key = ParserOps.ExtractBlock(ParsingString, 0, Marker);
		ParsingString.Remove(0, Key.Len() + 1);
		Value = ParsingString;
		(*GetOrPost)[Key] = Value;
	}
}

void WebServer::ProcessURI(const STString& CommandString)
{
	STString ParsingString, Marker, DocRoot, RequestingFile;
	STParserOps ParserOps;
	STDriveOps DriveOps;

	if((*_SERVER)["debugrequest"] == "on")
	{
		xlog->log("void WebServer::ProcessURI('%s')\n", (const char*)CommandString);
	}
	chdir((const char*)(*_SERVER)["DOCUMENT_ROOT"]);

	Marker = " ";
	ParsingString = ParserOps.ExtractBlock(CommandString, 1, Marker);
	if((*_SERVER)["debugrequest"] == "on")
	{
		xlog->log("REQUEST_URI = '%s'\n", (const char*)ParsingString);
	}
	if(ParsingString == "")	ParsingString = "/";

	(*_SERVER)["REQUEST_URI"] = ParsingString;
	if((*_SERVER)["REQUEST_URI"].Pos("/server:") == 0)
	{
		(*_SERVER)["SCRIPT_NAME"] = "/server:";
		PageContent->SetFileType(FT_INTERNAL);
	} else
	{
		Marker = "?";
		//ParserOps.SetDebugLevel(99);
		RequestingFile = ParserOps.ExtractBlock(ParsingString, 0, Marker);

		(*_SERVER)["SCRIPT_NAME"] = RequestingFile;
		if((*_SERVER)["debugrequest"] == "on")
		{
			xlog->log("RequestingFile = %s\n", (const char*)RequestingFile);
			xlog->log("(*_SERVER)[\"DOCUMENT_ROOT\"] = '%s'\n", (const char*)(*_SERVER)["DOCUMENT_ROOT"]);
		}
		DocRoot = (*_SERVER)["DOCUMENT_ROOT"];
		//xlog->log("Look for an existing directory %s%s in %s on line %d\n", (const char*)DocRoot, (const char*)RequestingFile, __FUNCTION__, __LINE__);
		if(DriveOps.DirExists(DocRoot + RequestingFile))
		{
			//xlog->log("Request for existing directory\n");
			// If only URL and a directory are requested.
			if(DriveOps.FileExists(DocRoot + RequestingFile + "index.so"))
			{
				RequestingFile += "index.so";
			} else
			if(DriveOps.FileExists(DocRoot + RequestingFile + "index.html"))
			{
				RequestingFile += "index.html";
			}
			(*_SERVER)["SCRIPT_NAME"] = RequestingFile;
			// xlog->log("new RequestingFile = %s\n", (const char*)RequestingFile);
		}/* else
		{
			xlog->log("No such directory %s%s in %s on line %d\n", (const char*)DocRoot, (const char*)RequestingFile, __FUNCTION__, __LINE__);
		}*/
		if(DriveOps.FileExists(DocRoot + RequestingFile))
		{
			PageContent->SetFileType(RequestingFile);
		} else
		{
			if(DriveOps.DirExists(DocRoot + RequestingFile))
			{
				// xlog->log("Directory exists, list it\n");
				// No default page and no pages chosen but request a valid directory, list the directory.
				(*_SERVER)["REQUEST_URI"] = RequestingFile;
				PageContent->SetFileType(FT_DIR_LISTING);
			} else
			{
				// Invalid path or file
				PageContent->SetError(HTTP_NOT_FOUND);
			}
		}
	}
	// This value is none standard but I want it here so I do not a have to keep coding a solution for it.
	(*_SERVER)["SCRIPT_DIR"] = (*_SERVER)["SCRIPT_NAME"];
	if(DriveOps.FileExists(DocRoot + (*_SERVER)["SCRIPT_DIR"]))
	{
		int LastSlash = (*_SERVER)["SCRIPT_DIR"].Pos("/", -1);
		(*_SERVER)["SCRIPT_DIR"].Remove(LastSlash + 1, (*_SERVER)["SCRIPT_DIR"].Len());
		//xlog->log("(*_SERVER)['SCRIPT_DIR'] = '%s'\n", (const char*)(*_SERVER)["SCRIPT_DIR"]);
	}
}

void WebServer::ListDirectory()
{
	PageContent->ListDirectory((*_SERVER)["REQUEST_URI"]);
}

int WebServer::Receive()
{
	STString ParsingString, ContentString;
	STParserOps ParserOps;
	STString IntToStr;
	int TempReturn = 0;
	char* InTcpBuffer;
	int TcpByteCount, TotalTcpByteCount;
	STString DataValue;
	int MaxUploadSize = (*_FILE)["MAX_FILE_SIZE"].ToInt() + 1024;
	int LookForMoreCounter = 20;
	TotalTcpByteCount = 0;
	bool StartMarker = true;
	TcpByteCount = 1;
	struct stBinaryPacket* BPacket;
	ClearRxBufferList();
	TotalTcpByteCount = 0;

	// BPacket = (struct stBinaryPacket*)malloc(sizeof(stBinaryPacket));
	BPacket = new struct stBinaryPacket;
	BPacket->Size = 0;
	// BPacket->Data = (char*)malloc(BUFFER_LIMIT + 1); // BINARY_PACKET_SIZE);
	BPacket->Data = new char[BUFFER_LIMIT + 1];
	// FILE* FilePointer;
	// FilePointer = fopen("/tmp/wtf.DebugFileSaver.txt", "wb");
	Socket->Listen();
	if((*_SERVER)["debugrequest"] == "on")
	{
		xlog->log("-------------------------- BEGIN INCOMMING REQUEST %d socket %d FROM %s --------------------------\n", RequestCounter, Socket->CurrentSocket(), (const char*)(*_SERVER)["REMOTE_ADDR"]);
	}
	int ReceieveCode = Socket->Receive(BPacket->Data, BUFFER_LIMIT, &TcpByteCount);
	if((*_SERVER)["debugrequest"] == "on")
	{
		fprintf(stderr, "ReceieveCode %d = Socket->Receive(BPacket->Data, BUFFER_LIMIT, &TcpByteCount); \n", ReceieveCode);
	}
	Socket->SetBlocking(false);
	while(ReceieveCode > 0)
	{
		if(StartMarker)
		{
			RequestCounter++;
			StartMarker = false;
		}
		BPacket->Data[TcpByteCount] = 0;
		TotalTcpByteCount += TcpByteCount;
		BPacket->Size = TcpByteCount;
		RxBufferList->Add(BPacket);
		//if(FilePointer)
		//{
		//	fputs((char *)BPacket->Data, FilePointer);
		//}
		if((*_SERVER)["debugrequest"] == "on")
		{
			// To see the header and requests uncomment these two lines.
			xlog->log("%s", BPacket->Data);
			fflush(NULL);
		}
		//BPacket = (struct stBinaryPacket*)malloc(sizeof(stBinaryPacket));
		BPacket = new struct stBinaryPacket;
		BPacket->Size = 0;
		// BPacket->Data = (char*)malloc(BUFFER_LIMIT + 1); // BINARY_PACKET_SIZE);
		//xlog->log("WebServer::Receive() look for more data coming in on the socket");
		BPacket->Data = new char[BUFFER_LIMIT + 1]; // BINARY_PACKET_SIZE);
		ReceieveCode = Socket->Receive(BPacket->Data, BUFFER_LIMIT, &TcpByteCount);
		//ReceieveCode = 0;
	}
	delete[] BPacket->Data;
	delete BPacket;
	if(TotalTcpByteCount > 0)
	{
		//if(FilePointer)
		//{
		//	fclose(FilePointer);
		//}

		if((*_SERVER)["debugrequest"] == "on")
		{
			fprintf(stderr, "TotalTcpByteCount = %d \n", TotalTcpByteCount);
		}
		if(TotalTcpByteCount > 0)
		{
			Request->Clear();
			_GET->Clear();
			_POST->Clear();
			if((*_SERVER)["debugrequest"] == "on")
			{
				xlog->log("\n------------------------\nReceived %d bytes of data\n------------------------\n", TotalTcpByteCount);
			}
			bool DoneParsingHeader = false;
			int EndHeaderMarker = 0;
			int SamplePosition;
			ParsingString = "";

			while(! DoneParsingHeader)
			{
				BPacket = (struct stBinaryPacket*)RxBufferList->Item(0);
				SamplePosition = 0;
				while((SamplePosition < BPacket->Size) && (! DoneParsingHeader))
				{
					if(BPacket->Data[SamplePosition] == '\r')
					{
						SamplePosition++;
						TotalTcpByteCount--;
					}
					if(BPacket->Data[SamplePosition] == '\n')
					{
						Request->Add(ParsingString);
						ParsingString = "";
						EndHeaderMarker++;
						TotalTcpByteCount--;
						if(EndHeaderMarker > 1)
						{
							SamplePosition++; // To skip the last \n
							DoneParsingHeader = true;
							// Down shift as long as it is not the last char in the buffer. If it is last char just delete the buffer.
							if(SamplePosition >= BPacket->Size - 1)
							{
								delete[] BPacket->Data;
								delete BPacket;
								RxBufferList->Delete(0);
								SamplePosition = 0;
								BPacket = 0;
							} else
							{
								// Keep the buffer but remove the data at the front that we have already sampled.
								BPacket->Size -= SamplePosition;
								for(int i = 0; i < BPacket->Size; i++)
								{
									BPacket->Data[i] = BPacket->Data[i+SamplePosition];
								}
							}
						}
					} else
					{
						EndHeaderMarker = 0;
						ParsingString += BPacket->Data[SamplePosition];
						TotalTcpByteCount--;
					}
					SamplePosition++;
					if(BPacket == 0) break;
				}
				// 2 reasons why here, buffer empty or end of header
				if(! DoneParsingHeader)
				{
					delete[] BPacket->Data;
					delete BPacket;
					RxBufferList->Delete(0);
					BPacket = 0;
					if(RxBufferList->Count() == 0)
					{
						DoneParsingHeader = true;
					}
				}
			}
			(*_SERVER)["REQUEST_METHOD"] = "GET";
			ContentString = Request->Item(0);
			ProcessURI(ContentString);
			ProcessGetPostData(ContentString, _GET);
			// check content type if text then.
			DataValue = GetData("Content-Type");
			//xlog->log("WebServer::Receive() line 526 DataValue for Content-Type = '%s'\n", (const char*)DataValue);
			if((DataValue == "") || (DataValue.Pos("application/x-www-form-urlencoded") == 0)) // assume text
			{
				if(ContentString.Pos("POST ") == 0)
				{
					ContentString = "";
					while(RxBufferList->Count() > 0)
					{
						BPacket = (struct stBinaryPacket*)RxBufferList->Item(0);
						for(int i = 0; i < BPacket->Size; i++)
						{
							ContentString += BPacket->Data[i];
						}
						delete[] BPacket->Data;
						delete BPacket;
						RxBufferList->Delete(0);
					}
					//xlog->log("ProcessGetPostData(ContentString, _POST);\n");
					ProcessGetPostData(ContentString, _POST);
					STString RebuildStr;
					for(int i = 0; i < _POST->Count(); i++)
					{
						RebuildStr = UrlDecode((*_POST)[i]);
						(*_POST)[i] = RebuildStr;
					}
				} else
				{
					// I think this is text but not a post, what is it?
					// xlog->log("Don't know what to do with the buffer\n");
				}
			}
			if(DataValue.Pos("multipart/form-data") == 0)
			{
				//xlog->log("WebServer::Receive() line 553 go parse the form data\n");
				STStringsEx Ex;
				STString LookForBoundary = DataValue;
				Ex.Upper(LookForBoundary);
				int PostionBoundary = LookForBoundary.Pos("BOUNDARY");
				DataValue.Remove(0, PostionBoundary);
				DataValue.Remove(0, DataValue.Pos("=") + 1);
				//xlog->log("these are the boundry markers = '%s'\n", (const char*)DataValue);
				ProcessFileUpload(DataValue);
			}
			// Look for cookies. We can ignore the GET line and the last 2 that will be blank or POST data.
			int i;
			//xlog->log("Request->Count() = %d\n", Request->Count());
			for(i = 1 ; i < Request->Count(); i++)
			{
				ContentString = Request->Item(i);
				//xlog->log("Request->Item(%d); = '%s'\n", i, (const char*)ContentString);
				if(ContentString.Pos("Cookie: ") == 0)
				{
					//xlog->log("ParseCookies(ContentString);");
					ParseCookies(ContentString);
				}
			}
			TempReturn = 1;
		}
		for(int i = 0; i < (*_POST).Count(); i++)
		{
			xlog->log("line 565 (*_POST)[%d] => '%s' = '%s'\n", i, (const char*)(*_POST).Key(i), (const char*)(*_POST)[i]);
		}
	} else
	{
		xlog->log("WebServer::Receive() we may have gotten a socket error so we are not going to honor this request, return(%d);", TempReturn);
	}
	return(TempReturn);
}

/****************************************************************
const char* WebServer::UrlEncode(const STString& OrigStr)
{
	Replace any chars 0-47, 58-64, 91-96 and 123-255 with hex values.
}

*****************************************************************/

const char* WebServer::UrlDecode(const STString& OrigStr)
{	
	STString RebuildString = OrigStr;
	//xlog->log("WebServer::UrlDecode('%s')\n", (const char*)OrigStr);
	int NextPos = RebuildString.Pos("+");
	char* MangleString = RebuildString;
	while(NextPos > 0)
	{
		MangleString[NextPos] = ' ';
		NextPos = RebuildString.Pos("+", NextPos);
	}
	//xlog->log("RebuildString = '%s'\n", (const char*)RebuildString);
	char NewHexValue;
	ReturnString = "";
	int OrigLength = RebuildString.Len();
	for(int i = 0; i < OrigLength; i++)
	{
		if(RebuildString[i] == '%')
		{
			//xlog->log("UrlDecode convert %%%c%c to ", RebuildString[i + 1], RebuildString[i + 2]);
			NewHexValue = 0;
			if(OrigLength > i + 2)
			{
				//xlog->log("RebuildString[i + 1] = %c", RebuildString[i + 1]);
				if((RebuildString[i + 1] >= '0') && (RebuildString[i + 1] <= '9'))
				{
					NewHexValue = RebuildString[i + 1] - '0';					
				}
				if((RebuildString[i + 1] >= 'A') && (RebuildString[i + 1] <= 'F'))
				{
					NewHexValue = RebuildString[i + 1] - 'A' + 10;					
				}
				if((RebuildString[i + 1] >= 'a') && (RebuildString[i + 1] <= 'f'))
				{
					NewHexValue = RebuildString[i + 1] - 'a' + 10;					
				}
				//xlog->log("%d = '%c'\n", NewHexValue, NewHexValue);
				NewHexValue <<= 4;
				//xlog->log("RebuildString[i + 2] = %c", RebuildString[i + 2]);
				if((RebuildString[i + 2] >= '0') && (RebuildString[i + 2] <= '9'))
				{
					NewHexValue += RebuildString[i + 2] - '0';					
				}
				if((RebuildString[i + 2] >= 'A') && (RebuildString[i + 2] <= 'F'))
				{
					NewHexValue += RebuildString[i + 2] - 'A' + 10;					
				}
				if((RebuildString[i + 2] >= 'a') && (RebuildString[i + 2] <= 'f'))
				{
					NewHexValue += RebuildString[i + 2] - 'a' + 10;					
				}
			}	
			//xlog->log("%d = '%c'\n", NewHexValue, NewHexValue);
			if(((NewHexValue >= 32) && (NewHexValue <= 126)) || (NewHexValue == 10) || (NewHexValue == 13))
			{				
				ReturnString += NewHexValue;
				i += 2;
			} else
			{
				//xlog->log("not going to convert that\n");
				// Could not safely turn into a Hex value, skip the % and move to the next char.
				ReturnString += RebuildString[i];
			}
		} else
		{
			ReturnString += RebuildString[i];
		}
	}
	return(ReturnString);
}

struct stBinaryPacket* WebServer::EatReturn(int *SamplePosition, struct stBinaryPacket* BPacket)
{
	// Eat the \r that does not belong.
	if(BPacket->Data[*SamplePosition] == '\r')
	{
		(*SamplePosition)++;
	}
	BPacket = ShiftOutBuffer(SamplePosition, BPacket);
	return(BPacket);
}

struct stBinaryPacket* WebServer::EatLine(int *SamplePosition, struct stBinaryPacket* BPacket)
{
	int LookForCRLF = 0;
	// Eat everything upto the EOL.
	while((BPacket->Data[*SamplePosition] != '\n') && (BPacket->Size > 0))
	{
		if((LookForCRLF == 0) && (BPacket->Data[*SamplePosition] == '\r'))
		{
			LookForCRLF = 1;
		} else
		{
			LookForCRLF = 0;
		}
		(*SamplePosition)++;
		BPacket = ShiftOutBuffer(SamplePosition, BPacket);
	}
	if((LookForCRLF == 1) && (BPacket->Size > 0))
	{
		ClientBrowserSendingCRLF = true;
	}

	if(BPacket->Size > 0)
	{
		// Eat the EOL.
		(*SamplePosition)++;
		BPacket = ShiftOutBuffer(SamplePosition, BPacket);
	}
	return(BPacket);
}

struct stBinaryPacket* WebServer::ShiftOutBuffer(int *SamplePosition, struct stBinaryPacket* BPacket)
{
	if(*SamplePosition >= BPacket->Size)
	{
		*SamplePosition = 0;
		if(BPacket > 0)
		{
			delete[] BPacket->Data;
			delete BPacket;
			RxBufferList->Delete(0);
			BPacket = 0;
			if(RxBufferList->Count() > 0)
			{
				BPacket = (struct stBinaryPacket*)RxBufferList->Item(0);
			}
		}
	}
	return(BPacket);
}

void WebServer::ParseInfoOnUploadedFile(STStringList* UploadInfo)
{
	// Example of the UploadInfo content but we only want the form-data
	// Content-Disposition: form-data; name="FileToUpload"; filename="ARM boot settings.txt~"
	// Content-Type: application/octet-stream
	STString EachLine, UpperEachLine;
	STStringsEx StringEx;
	STHash DataHash;

	for(int i = 0; i < UploadInfo->Count(); i++)
	{
		EachLine = UploadInfo->Item(0);
		UploadInfo->Delete(0);
		UpperEachLine = EachLine;
		StringEx.Upper(UpperEachLine);
		if(UpperEachLine.Pos("CONTENT-DISPOSITION: FORM-DATA") > -1)
		{
			DataSplit(EachLine, &DataHash);
			(*_FILE)["FILE_NAME"] = DataHash["filename"];
			(*_FILE)["FORM_NAME"] = DataHash["name"];
		}
	}
}

void WebServer::DataSplit(const STString& SpitLine, STHash* FillHash)
{
	FillHash->Clear();
	STStringList Sections;
	STStringList Parts;
	STString CleanText;

	Split(SpitLine, ";", &Sections);
	while(Sections.Count() > 0)
	{
		Split(Sections.Item(0), "=", &Parts);
		Sections.Delete(0);
		if(Parts.Count() > 1)
		{
			CleanText = Parts.Item(1).Strip();
			CleanText = CleanText.Mid(1, CleanText.Len() - 2);
			Parts.Item(0).Strip();
			(*FillHash)[Parts.Item(0)] = CleanText;
		}
	}
}

void WebServer::Split(const STString& StringToSpit, const char* SplitMarker, STStringList* FillParts)
{
	STString CuttingString, WorkStr;
	FillParts->Clear();
	WorkStr = StringToSpit;
	while(WorkStr.Pos(SplitMarker) > -1)
	{
		CuttingString = WorkStr.Left(WorkStr.Pos(SplitMarker));
		CuttingString.Strip();
		FillParts->Add(CuttingString);
		WorkStr.Remove(0, WorkStr.Pos(SplitMarker) + 1);
	}
	if(WorkStr.Len() > 0)
	{
		WorkStr.Strip();
		FillParts->Add(WorkStr);
	}
}

void WebServer::ProcessFileUpload(const STString& BoundaryString)
{
	// find the boundary, remove anything before the first boundary and after the last one.
	// split the data block into many data blocks devided by boundary markers, process each block one at a time.
	unsigned int i;
	int DosFormatBountry = 0;
	struct stBinaryPacket* BPacket;

	// Get the length of the buffer
	unsigned int BufferSize = 0;
	STString WorkingString;
	//xlog->log("\n\n\n\n\n\n\n\n\n\n\n\n----------------- WHAT'S IN THE BUFFER -----------------\n");
	for(i = 0; i < RxBufferList->Count(); i++)
	{
		BPacket = (struct stBinaryPacket*)RxBufferList->Item(i);
		BufferSize += BPacket->Size;
		//xlog->log("%s", BPacket->Data);
	}
	//xlog->log("----------------- DONE LOOKING AT WHAT'S IN THE BUFFER -----------------\n\n\n\n\n\n\n\n\n\n\n\n\n");

	// The upload could be very large, using strings would cause a lot of CPU and memory usage to keep re-allocating a larger and larger buffer.
	// This is to make it more efficent.
	//xlog->log("BufferSize = %d\n", BufferSize);
	// char* TempBuffer = (char*)malloc(BufferSize);
	char* TempBuffer = new char[BufferSize];
	unsigned int BufferPosition = 0;
	while(RxBufferList->Count() > 0)
	{
		//xlog->log("Populating buffer\n");
		BPacket = (struct stBinaryPacket*)RxBufferList->Item(0);
		for(i = 0; i < BPacket->Size; i++)
		{
			TempBuffer[BufferPosition + i] = BPacket->Data[i];
		}
		BufferPosition += BPacket->Size;
		delete[] BPacket->Data;
		delete BPacket;
		RxBufferList->Delete(0);
	}
	WorkingString = TempBuffer;
	delete[] TempBuffer;
	// Remove end boundary marker
	//STString EndBoundry = BoundaryString;
	//EndBoundry += "--";
	//WorkingString.Remove(WorkingString.Pos(EndBoundry), WorkingString.Len());

	int NextPosition = WorkingString.Pos(BoundaryString);
	if(WorkingString[NextPosition + BoundaryString.Len()] == '\r')
	{
		DosFormatBountry = 1;
	}
	// xlog->log("WebServer::ProcessFileUpload line 757 NextPosition = %d\n", NextPosition);
	if(NextPosition > -1)
	{
		// if(WorkingString[NextPosition + BoundaryString.Len()] == '\r') NextPosition++;
		// Srip off starting boundary marker
		WorkingString.Remove(0, NextPosition + BoundaryString.Len() + 1 + DosFormatBountry);
		STStringList MultiParts;
		STString StripString;
		int WorkingStringLen, BoundryPaddingOffset;

		while(WorkingString.Len() > 0)
		{
			BoundryPaddingOffset = 1 + DosFormatBountry;
			NextPosition = WorkingString.Pos(BoundaryString);
			if((WorkingString[NextPosition - 1] == '-') && (WorkingString[NextPosition - 2] == '-'))
			{
				BoundryPaddingOffset += 2;
			}
			WorkingStringLen = WorkingString.Len();
			StripString = WorkingString.Left(NextPosition - BoundryPaddingOffset);
			if(StripString.Len() > 0) MultiParts.Add(StripString);
			WorkingString.Remove(0, NextPosition + BoundaryString.Len() + 1 + DosFormatBountry);
		}

		STString WorkingBlock;
		STString BlockTagDisposition;
		STString BlockTagType;
		STStringsEx StringEx;
		STString UpperText;
		STHash DataHash;
		STString Key, Value;
		for(int EachBlock = 0; EachBlock < MultiParts.Count(); EachBlock++)
		{
			WorkingBlock = MultiParts.Item(EachBlock);
			// xlog->log("WebServer::ProcessFileUpload line 795 WorkingBlock = '%s'\n", (const char*)WorkingBlock);
			if(DosFormatBountry)
			{
				// xlog->log("WorkingBlock.Pos(\\r) = %d\n", WorkingBlock.Pos("\r"));
				BlockTagDisposition = WorkingBlock.Left(WorkingBlock.Pos("\r"));
			} else
			{
				// xlog->log("WorkingBlock.Pos(\\n) = %d\n", WorkingBlock.Pos("\n"));
				BlockTagDisposition = WorkingBlock.Left(WorkingBlock.Pos("\n"));
			}
			// xlog->log("BlockTagDisposition.Len() %d, BlockTagDisposition = '%s'\n", BlockTagDisposition.Len(), (const char*)BlockTagDisposition);
			WorkingBlock.Remove(0, WorkingBlock.Pos("\n") + 1);
			// BlockTagType may not be there, if not then it will be the blank line before the block.
			// xlog->log("WebServer::ProcessFileUpload line 808 WorkingBlock = '%s' WorkingBlock.Pos('\n')\n", (const char*)WorkingBlock, WorkingBlock.Pos("\n"));
			BlockTagType = WorkingBlock.Left(WorkingBlock.Pos("\n"));
			// xlog->log("WebServer::ProcessFileUpload line 810 BlockTagType.Len() = %d, BlockTagType = '%s'\n", BlockTagType.Len(), (const char*)BlockTagType);
			BlockTagType.Strip();
			// xlog->log("WebServer::ProcessFileUpload line 812 BlockTagType.Len() = %d, BlockTagType stripped = '%s'\n", BlockTagType.Len(), (const char*)BlockTagType);
			WorkingBlock.Remove(0, WorkingBlock.Pos("\n") + 1);
			// xlog->log("WebServer::ProcessFileUpload line 814 BlockTagDisposition.Len() = %d\n", BlockTagDisposition.Len());
			if(BlockTagDisposition.Len() > 0)
			{
				DataSplit(BlockTagDisposition, &DataHash);
				// xlog->log("WebServer::ProcessFileUpload line 818 BlockTagType = '%s'\n", (const char*)BlockTagType);
				if(BlockTagType != "")
				{
					WorkingBlock.Remove(0, WorkingBlock.Pos("\n") + 1);
					UpperText = BlockTagType;
					StringEx.Upper(UpperText);
					if(UpperText.Pos("CONTENT-TYPE: ") > -1)
					{
						(*_FILE)["FILE_NAME"] = DataHash["filename"];
						(*_FILE)["FORM_NAME"] = DataHash["name"];
						(*_FILE)["SIZE"].Sprintf("%d", WorkingBlock.Len());
						STSystemAPI SystemAPI;
						system("rm -f /tmp/ebws_upload*");
						FILE* FilePointer;
						(*_FILE)["TEMP_FILE_NAME"] = SystemAPI.GetTempFileName("ebws_upload");
						FilePointer = fopen((const char*)(*_FILE)["TEMP_FILE_NAME"], "wb");
						if(FilePointer)
						{
							fputs((char *)WorkingBlock, FilePointer);
							fclose(FilePointer);
						}
					}
				} else
				{
					Key = DataHash["name"];
					// xlog->log("void WebServer::ProcessFileUpload line 842\n(*_POST)[%s] = '%s'\n", (const char*)Key, (const char*)WorkingBlock);
					(*_POST)[Key] = WorkingBlock;
					/*for(int i = 0; i < (*_POST).Count(); i++)
					{
						xlog->log("(*_POST)[%d] => '%s' = '%s'\n", i, (const char*)(*_POST).Key(i), (const char*)(*_POST)[i]);
					}*/
				}
			}
		}
	}
}

// This worked great for one file upload but multipart/form-data can also used when a form submits more then one type of data that may or may not include a file.
/*void WebServer::Old_ProcessFileUpload(const STString& BoundaryString)
{
	exit(85); // do not call this function. not used, old way. un-commented for syntax highlight reasons.
	BoundaryString = "--" + BoundaryString;
	STStringList UploadInfo;
	bool DoneParsing = false;
	int EndHeaderMarker = 0;
	int SamplePosition;
	int DataFieldZone = 0; // 0=before boundary, 1=before data, 2=in data, 3=after data
	STString ParsingString;
	int TotalUploadSize = 0;
	int IndexMatchingBoundary = 0;
	struct stBinaryPacket* BPacket;
	FILE* FilePointer;
	STString HoldForClosingMarker;
	int UploadFileSize = 0;

	ClientBrowserSendingCRLF = false;
	int LastDataZone = -1;
	STSystemAPI SystemAPI;
	system("rm -f /tmp/ebws_upload*");
	(*_FILE)["TEMP_FILE_NAME"] = SystemAPI.GetTempFileName("ebws_upload");
	// xlog->log("(*_FILE)['TEMP_FILE_NAME'] = '%s'\n", (const char*)(*_FILE)["TEMP_FILE_NAME"]);
	(*_FILE)["SIZE"] = "0";

	while(! DoneParsing)
	{
		BPacket = (struct stBinaryPacket*)RxBufferList->Item(0);
		SamplePosition = 0;
		while((! DoneParsing) && (SamplePosition < BPacket->Size))
		{
			if(LastDataZone != DataFieldZone)
			{
				LastDataZone = DataFieldZone;
				// xlog->log("DataFieldZone == %d\n", DataFieldZone);
			}
			// Looking for the boundry marker line. Start of the upload.
			if(DataFieldZone == 0)
			{
				if(BPacket->Data[SamplePosition] == BoundaryString[IndexMatchingBoundary])
				{
					IndexMatchingBoundary++;
					if(IndexMatchingBoundary >= BoundaryString.Len())
					{
						SamplePosition++;
						BPacket = ShiftOutBuffer(&SamplePosition, BPacket);
						DataFieldZone = 1;
						// Keep the buffer but remove the data at the front that we have already sampled.
						BPacket->Size -= SamplePosition;
						for(int i = 0; i < BPacket->Size; i++)
						{
							BPacket->Data[i] = BPacket->Data[i+SamplePosition];
						}
						SamplePosition = 0;
						BPacket = EatLine(&SamplePosition, BPacket);
					}
				} else
				{
					IndexMatchingBoundary = 0;
				}
				if(DataFieldZone == 0) SamplePosition++;
				BPacket = ShiftOutBuffer(&SamplePosition, BPacket);
			}
			// Just after the boundary start marker line. This is the contents information of the upload.
			if(DataFieldZone == 1)
			{
				BPacket = EatReturn(&SamplePosition, BPacket);
				if(BPacket->Data[SamplePosition] == '\n')
				{
					UploadInfo.Add(ParsingString); // The header is text info about the uploaded file
					// xlog->log("ParsingString = '%s'\n", (const char*)ParsingString);
					ParsingString = "";
					SamplePosition++;
					BPacket = EatReturn(&SamplePosition, BPacket);
					if(BPacket->Data[SamplePosition] == '\n')
					{
						SamplePosition++; // To skip the last \n
						BoundaryString = "\n" + BoundaryString;
						if(ClientBrowserSendingCRLF) BoundaryString = "\r" + BoundaryString;
						// Down shift as long as it is not the last char in the buffer. If it is last char just delete the buffer.
						DataFieldZone = 2;
						IndexMatchingBoundary = 0;
						HoldForClosingMarker = "";
						// xlog->log("(*_FILE)['TEMP_FILE_NAME'] = '%s'\n", (const char*)(*_FILE)["TEMP_FILE_NAME"]);
						FilePointer = fopen((const char*)(*_FILE)["TEMP_FILE_NAME"], "wb");
						if(! FilePointer)
						{
							DataFieldZone = 3; // Zone 3 does cleanup
							DataFieldZone = 1; // Don't allow anymore processing in this while loop.
							STString ErrStr = "ERROR 140: Could not create temporary file ";
							ErrStr += (*_FILE)["TEMP_FILE_NAME"];
							PageContent->Log((const char*)ErrStr);
						}
						BPacket = ShiftOutBuffer(&SamplePosition, BPacket);
					}
				} else
				{
					EndHeaderMarker = 0;
					ParsingString += BPacket->Data[SamplePosition++];
					BPacket = ShiftOutBuffer(&SamplePosition, BPacket);
				}
			}
			// Save all data from here on into a binary location (BPacket) util we find the Boundry marker that indicates the end of the upload.
			if(DataFieldZone == 2)
			{
				/*************************************************
				* The ending marker is preceded by \n or \r\n. The sample buffer may split between them, must look ahead to see if the boundry marker is in position.
				* Although not common it is possible that a block of text could exits in the upload that would look like the start of the boundry marker.
				* Weather it is the boundry marker or not will not be known until the text failes to match, indicating it is part of the upload body, or it matches,
				* indicating that the upload is done.
				*
				************************************************** /
				if(BPacket->Data[SamplePosition] == BoundaryString[IndexMatchingBoundary])
				{
					IndexMatchingBoundary++;
					if(IndexMatchingBoundary >= BoundaryString.Len())
					{
						DataFieldZone = 3;
						if(FilePointer) fclose(FilePointer);
						(*_FILE)["SIZE"].Sprintf("%d", UploadFileSize);
						ParseInfoOnUploadedFile(&UploadInfo);
					} else
					{
						HoldForClosingMarker += BPacket->Data[SamplePosition];
					}
				} else
				{
					IndexMatchingBoundary = 0;
					while(HoldForClosingMarker.Len() > 0)
					{
						UploadFileSize++;
						if(FilePointer) fputc(HoldForClosingMarker[0], FilePointer);
						//xlog->log("%c" , HoldForClosingMarker[0]);
						//fflush(NULL);
						HoldForClosingMarker.Remove(0, 1);
					}
					// If the previous string in HoldForClosingMarker looked like the start of the boundry marker, this next char could be the real start of the boundry marker.
					if(BPacket->Data[SamplePosition] != BoundaryString[IndexMatchingBoundary])
					{
						UploadFileSize++;
						if(FilePointer) fputc(BPacket->Data[SamplePosition], FilePointer);
						//xlog->log("%c" , BPacket->Data[SamplePosition]);
						//fflush(NULL);
					}
				}
				SamplePosition++;
				BPacket = ShiftOutBuffer(&SamplePosition, BPacket);
			}
			// This is cleanup. Used and end of upload or if there was an error.
			if(DataFieldZone == 3)
			{
				while(RxBufferList->Count() > 0)
				{
					BPacket = (struct stBinaryPacket*)RxBufferList->Item(0);
					delete[] BPacket->Data;
					delete BPacket;
					RxBufferList->Delete(0);
				}
				DoneParsing = true;
			}
			// If we run out of data then there is nothing left to do.
			if(RxBufferList->Count() == 0)
			{
				// xlog->log("RxBufferList->Count() == 0\n");
				DoneParsing = true;
			}
		}
	}
} */

STString WebServer::GetData(const char* DataField)
{
	STString SampleLine;
	STString Search;
	STStringsEx Ex;
	Search = DataField;
	Ex.Upper(Search);
	Search += ":";
	ReturnString = "";
	//xlog->log("STString WebServer::GetData(const char* DataField)\n");
	for(int i = 0; i < Request->Count(); i++)
	{
		SampleLine = Request->Item(i);
		Ex.Upper(SampleLine);
		if(SampleLine.Pos(Search) == 0)
		{
			ReturnString = Request->Item(i);
			ReturnString.Remove(0, Search.Len());
			while(ReturnString[0] == ' ')
			{
				ReturnString.Remove(0, 1);
			}
			break;
		}
	}
	return(ReturnString);
}
