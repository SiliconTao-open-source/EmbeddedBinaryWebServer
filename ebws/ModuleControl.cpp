#include "ModuleControl.h"

ModuleControl::ModuleControl(const char *ModuleName, void *HandleLibPointer)
{
	ModPointer = HandleLibPointer;
	ModName = ModuleName;
}

ModuleControl::ModuleControl(const STString& ModuleName, void *HandleLibPointer)
{
	ModPointer = HandleLibPointer;
	ModName = ModuleName;
}

STString ModuleControl::Name()
{
	return(ModName);
}

void *ModuleControl::Pointer()
{
	return(ModPointer);
}
