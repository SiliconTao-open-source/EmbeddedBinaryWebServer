#ifndef included_ModuleControl
#define included_ModuleControl

#include <STString.h>

class ModuleControl
{
	public:
		ModuleControl(const char *ModuleName, void *HandleLibPointer);
		ModuleControl(const STString& ModuleName, void *HandleLibPointer);
		
		STString Name();
		void *Pointer();
	
	private:
		void *ModPointer;
		STString ModName;
};


#endif // included_ModuleControl
