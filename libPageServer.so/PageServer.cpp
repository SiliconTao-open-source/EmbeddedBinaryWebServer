#include "PageServer.h"
#include <time.h>
#include <STXlog.h>
#include <STSystemAPI.h>
#include <SocketServer.h>
#include <STDynamicDataLink.h>
#include <STStringsEx.h>
#include <stdio.h>
#include <stdlib.h>
#include <STDriveOps.h>
#include <STTime.h>
#include <STString.h>
#include <STStringList.h>
#include <STHash.h>
#include <dlfcn.h>


#define BINARY_PACKET_SIZE 8192

struct LibraryNode
{
	char* PahtName;
	void* LibPointer;
	void* (*InitializeFP)();		// FP is Function Pointer
	int (*ObjectEntryFP)(void*, void*, void*);
	void (*CloseFP)(void*);
	void* PageObject;
};

PageServer::PageServer(SocketServer* SocketPorinter, STHash* PGlobalVars, STXlog* NewXlog)
{
	xlog = NewXlog;
	Link = NULL;
	ModuleList = new STList();
	GlobalVars = PGlobalVars;
	Socket = SocketPorinter;	

	BinaryPacketList = new STList();
	MessageBody = "";
	MimeType = "";	
	ReturnString = "";
}
	
PageServer::~PageServer()
{
	while(ModuleList->Count())
	{
		struct LibraryNode* Ln = (struct LibraryNode*)ModuleList->Item(0);
		delete[] Ln->PahtName;
		Ln->CloseFP(Ln->PageObject);
		dlclose(Ln->LibPointer);		
		ModuleList->Delete(0);
	}
	delete ModuleList;
	ClearBinaryPacketList();
	delete BinaryPacketList;	
	// fprintf(stderr, "EBWS is about to delete the Link\n");
	if(Link) delete Link;
}

void PageServer::Endorse(STHash* HashPointer, const char* HashName)
{
	STString TempStr = HashName;
	Endorse(HashPointer, TempStr);
}

STDynamicDataLink* PageServer::GetDataLink()
{
	// Only activate this object if needed. 
	if(Link) return(Link);		
	
	if( (*_GLOBAL)["debuglink"] == "on")
	{
		Link = new STDynamicDataLink(true);
	} else
	{
		Link = new STDynamicDataLink();
	}
	Link->EnableStaticLink();
	Link->NeverTheHSC();
	return(Link);
}

void PageServer::Disavow(const char* HashName)
{
	//xlog->log("void PageServer::Disavow('%s') = '%s'\n- GlobalVars->Count = %d\n", (const char*)HashName, (const char*)(*GlobalVars)[HashName], GlobalVars->Count());	
	GlobalVars->Delete(HashName);
	//xlog->log("= GlobalVars->Count = %d\n", GlobalVars->Count());
}

void PageServer::Endorse(STHash *HashPointer, const STString& HashName)
{
	STString Convert;
	
	//xlog->log("+ GlobalVars->Count = %d\n", GlobalVars->Count());
	(*GlobalVars)[HashName] = Convert.Sprintf("%u", (unsigned int)HashPointer);
	//xlog->log("= GlobalVars->Count = %d\n", GlobalVars->Count());
	
	//xlog->log("PageServer::Endorse GlobalVars)['%s'] = (unsigned int)%u\n", (const char*)HashName, (unsigned int)HashPointer);
	//xlog->log("< GlobalVars->Count = %d\n", GlobalVars->Count());
	//xlog->log("< GlobalVars->Key(%d) = '%s'\n", GlobalVars->Count() - 1, (const char*)GlobalVars->Key(GlobalVars->Count() - 1));
	//fprintf(stderr, "GlobalVars[ x ] = '%s'\n", (const char*)(*GlobalVars)[GlobalVars->Count() - 1]);
	if(HashName == "_SERVER") _SERVER = (STHash*)(*GlobalVars)["_SERVER"].ToInt();		
	if(HashName == "_GET") _GET = (STHash*)(*GlobalVars)["_GET"].ToInt();
	if(HashName == "_POST") _POST = (STHash*)(*GlobalVars)["_POST"].ToInt();
	if(HashName == "_COOKIE") _COOKIE = (STHash*)(*GlobalVars)["_COOKIE"].ToInt();	
	if(HashName == "_GLOBAL") _GLOBAL = (STHash*)(*GlobalVars)["_GLOBAL"].ToInt();	
	if(HashName == "_FILE") _FILE = (STHash*)(*GlobalVars)["_FILE"].ToInt();	
}

int PageServer::ExecuteLibrary(const STString& LibName)
{	
	//xlog->log("void WebServer::ExecuteLibrary()\n");	
	// open the library and enter via standard call
	
	// Look to see if the library is already loaded.
	
	//xlog->log("void *WebServer::LoadLibrary()\n");	
	//LibPointer = 0;
	
	void *GenericPointer;
	
	STString TestString;
	STStringsEx Ex;
	STString BuildName;
	BuildName = Ex.StripClean(LibName);
	// (*_SERVER)["SCRIPT_NAME"]);
	while(BuildName.Pos("..") > -1)
	{
		BuildName.Remove(BuildName.Pos(".."), 1);
	}
	while(BuildName.Pos("./") > -1)
	{
		BuildName.Remove(BuildName.Pos("./"), 1);
	}
	while(BuildName.Pos("//") > -1)
	{
		BuildName.Remove(BuildName.Pos("//"), 1);
	}
	BuildName = "/" + BuildName;
	
	STString LibPathFile = (*_SERVER)["DOCUMENT_ROOT"] + BuildName;
	struct LibraryNode* Ln = 0;
	for(int i = 0; i < ModuleList->Count(); i++)
	{
		Ln = (struct LibraryNode*)ModuleList->Item(i);
		TestString = Ln->PahtName;
		if(TestString == LibPathFile)
		{			
			break;
		} else
		{
			Ln = 0;	
		}
	}
	
	if(Ln == 0)
	{
		//xlog->log("Load a library of file = '%s'\n", (const char *)LibPathFile);
		void* LibPointer = dlopen((const char*)LibPathFile, RTLD_LAZY);
		if(! LibPointer)
		{
			SetError(HTTP_ERROR);
			Log("Error loading library. No such file '%s'\n", (const char*)LibPathFile);	
			EPost("Error loading library. No such file '%s'<BR>", (const char*)LibPathFile);	
		} else
		{
			Ln = new struct LibraryNode;
			ModuleList->Add(Ln);	
			Ln->PahtName = new char[LibPathFile.Len() + 1];
			for(int i = 0; i < LibPathFile.Len() + 1; i++)
			{
				Ln->PahtName[i] = LibPathFile[i];
			}
			Ln->LibPointer = LibPointer;
			Ln->PageObject = 0;
			
			GenericPointer = dlsym(LibPointer, "Initialize");
			if(GenericPointer)
			{
				Ln->InitializeFP = (void* (*)())GenericPointer;
				Ln->PageObject = Ln->InitializeFP();
			} 
			
			GenericPointer = dlsym(LibPointer, "ObjectEntry");
			if(GenericPointer && Ln->PageObject) 
			{
				Ln->ObjectEntryFP = (int (*)(void*, void*, void*))GenericPointer;
			}
			
			GenericPointer = dlsym(LibPointer, "Close");
			if(GenericPointer && Ln->PageObject) 
			{
				Ln->CloseFP = (void (*)(void*))GenericPointer;
			}		
		}	
	}
	int ReturnInt = 0;
	if(Ln > 0)
	{		
		ReturnInt = Ln->ObjectEntryFP(Ln->PageObject, this, GlobalVars);
	}
	return(ReturnInt);
}

void PageServer::PostFromFilePre(const STString& FileName)
{
	STString RebuildString;
	STDriveOps DriveOps;
	STStringList FileData;
	
	// xlog->log("void PageServer::PostFromFilePre(%s)\n", (const char*)FileName);
	if(DriveOps.FileExists((*_SERVER)["DOCUMENT_ROOT"] + FileName))
	{
		FileData.LoadFromFile((*_SERVER)["DOCUMENT_ROOT"] + FileName);		
		if((*_SERVER)["debugpost"] == "on")
		{		
			xlog->log("void PageServer::PostFromFilePre('%s')\n", (const char*)FileName);
			xlog->log("FileData.LoadFromFile((*_SERVER)[\"DOCUMENT_ROOT\"] + %s)\n", (const char*)FileName);
			xlog->log("FileData.MemCount() = %d\n", FileData.MemCount());
		}
		Post("\n<PRE>\n");
		int i;
		for(i = 0; i < FileData.Count(); i++)
		{
			RebuildString = FileData.Item(i);
			Replace(&RebuildString, "&", "&amp;");
			Replace(&RebuildString, "<", "&lt;");
			Replace(&RebuildString, ">", "&gt;");
			Post(RebuildString + "\n");
		}
		Post("</PRE>\n");
	} else
	{
		xlog->log("Faild to post from missing file %s%s\n", (const char*)(*_SERVER)["DOCUMENT_ROOT"], (const char*)FileName);
	}
}

void PageServer::Replace(STString* RebuildString, const STString& Match, const char* NewStr)
{
	STString NewString = NewStr;
	Replace(RebuildString, Match, NewString);
}

void PageServer::Replace(STString* RebuildString, const char* Match, const STString& NewStr)
{
	STString TempStr = Match;
	Replace(RebuildString, TempStr, NewStr);
}

void PageServer::Replace(STString* RebuildString, const char* Match, const char* NewStr)
{
	STString MatchStr = Match;
	STString NewString = NewStr;
	Replace(RebuildString, MatchStr, NewString);
}

void PageServer::Replace(STString* RebuildString, const STString& Match, const STString& NewStr)
{
	int NextPosition;
	int Modified = 0;
	int BeyondLastPosition = 0;
	STString FrontStr, EndStr, Working;
	
	Working = *RebuildString;
	NextPosition = Working.Pos(Match);
	while(NextPosition > -1)
	{
		Modified = 1;
		NextPosition = Working.Pos(Match, BeyondLastPosition);
		if(NextPosition > -1)
		{
			FrontStr = Working;
			EndStr = Working;
			FrontStr.Remove(NextPosition, Working.Len());
			EndStr.Remove(0, NextPosition + Match.Len());
			Working = FrontStr + NewStr + EndStr;
			BeyondLastPosition = NextPosition + 1 + NewStr.Len();
			NextPosition = Working.Pos(Match, BeyondLastPosition);
		}
	}
	if(Modified == 1)
	{
		*RebuildString = Working;
	}
}

void PageServer::EvalFile(const STString& FileName)
{
	STString RebuildString, LookFor, ReplaceWith;
	STDriveOps DriveOps;
	STStringList FileData;
	int i, v, l;
	STHash *TempHash;
	
	//xlog->log("> GlobalVars->Count = %d\n", GlobalVars->Count());
	//xlog->log("void PageServer::EvalFile(%s)\n", (const char*)FileName);
	if(DriveOps.FileExists((*_SERVER)["DOCUMENT_ROOT"] + FileName))
	{
		FileData.LoadFromFile((*_SERVER)["DOCUMENT_ROOT"] + FileName);		
		if((*_SERVER)["debugpost"] == "on")
		{		
			xlog->log("void PageServer::EvalFile('%s')\n", (const char*)FileName);
			xlog->log("FileData.LoadFromFile((*_SERVER)[\"DOCUMENT_ROOT\"] + %s)\n", (const char*)FileName);
			xlog->log("FileData.MemCount() = %d\n", FileData.MemCount());
		}
		for(l = 0; l < FileData.Count(); l++)
		{
			RebuildString = FileData.Item(l);
			// Look for each variable in the string.
			//xlog->log("PageServer::EvalFile('%s') GlobalVars->Count() = %d\n", (const char*)FileName, GlobalVars->Count());
			if(RebuildString.Pos("$") > -1)
			{
				for( v = 0; v < GlobalVars->Count(); v++)
				{
					//xlog->log("GlobalVars->Key(%d) = '%s'\n", v, (const char*)GlobalVars->Key(v));
					//xlog->log("GlobalVars['%s'] = '%s'\n", (const char*)GlobalVars->Key(v), (const char*)(*GlobalVars)[v]);
					TempHash = (STHash *)(*GlobalVars)[GlobalVars->Key(v)].ToInt();
					// xlog->log("   TempHash @ 0x%08X = (STHash *)(*GlobalVars)['%s'].ToInt();\n", (int)TempHash, (const char*)GlobalVars->Key(v));
					for(i = 0; i < TempHash->Count(); i++)
					{
						//xlog->log("      TempHash->Count() %d\n", TempHash->Count());
						//xlog->log("      TempHash->Key(%d) = '%s'\n", i, (const char*)TempHash->Key(i));
						LookFor = "$" + GlobalVars->Key(v) + "['" + TempHash->Key(i) + "']";
						//fprintf(stderr, "PageServer::EvalFile LookFor = '%s'\n", (const char*)LookFor);
						ReplaceWith = (*TempHash)[TempHash->Key(i)];
						Replace(&RebuildString, LookFor, ReplaceWith);
					}
				}		
			}
			Post(RebuildString + "\n");
		}
	} else
	{
		xlog->log("Error EvalFile(%s), no such file.\n", (const char*)((*_SERVER)["DOCUMENT_ROOT"] + FileName));
	}
	xlog->log("Done EvalFile");
}
	
void PageServer::EvalPosted()
{
	STString LookFor, ReplaceWith;
	STHash *TempHash;
	int i, v;
	for( v = 0; v < GlobalVars->Count(); v++)
	{
		TempHash = (STHash *)(*GlobalVars)[GlobalVars->Key(v)].ToInt();
		for(i = 0; i < TempHash->Count(); i++)
		{
			LookFor = "$" + GlobalVars->Key(v) + "['" + TempHash->Key(i) + "']";
			ReplaceWith = (*TempHash)[TempHash->Key(i)];
			Replace(&MessageBody, LookFor, ReplaceWith);
		}
	}		
}

void PageServer::PostFromFile(const STString& FileName)
{
	STDriveOps DriveOps;
	STStringList FileData;
	
	//xlog->log("void PageServer::PostFromFile(%s)\n", (const char*)FileName);
	if(DriveOps.FileExists((*_SERVER)["DOCUMENT_ROOT"] + FileName))
	{
		FileData.LoadFromFile((*_SERVER)["DOCUMENT_ROOT"] + FileName);		
		if((*_SERVER)["debugpost"] == "on")
		{		
			xlog->log("void PageServer::PostFromFile('%s')\n", (const char*)FileName);
			xlog->log("FileData.LoadFromFile((*_SERVER)[\"DOCUMENT_ROOT\"] + %s)\n", (const char*)FileName);
			xlog->log("FileData.MemCount() = %d\n", FileData.MemCount());
		}
		while(FileData.Count() > 0)
		{
			// Post(FileData.Item(0) + "\n");
			MessageBody += FileData.Item(0) + "\n";
			//xlog->log(".");
			//fflush(NULL);
			FileData.Delete(0);
		}
	} else
	{
		xlog->log("Faild to post from missing file %s\n", (const char*)((*_SERVER)["DOCUMENT_ROOT"] + FileName));
	}
}

void PageServer::IPostFromFile(const STString& FileName)
{
	PostFromFile(FileName);
	SetFileType(FT_INCLUDE_SCRIPT);
}

int PageServer::GetBinarySize()
{
	struct stBinaryPacket* BPacket;
	int ContentLen = 0;
	for(int i = 0; i < BinaryPacketList->Count(); i++)
	{
		BPacket = (struct stBinaryPacket*)BinaryPacketList->Item(i);
		ContentLen += BPacket->Size;
	}
	return(ContentLen);
}

void PageServer::PostPage()
{
	// xlog->log("void PageServer::PostPage()\n");
	
	if(BinaryPacketList->Count() > 0)
	{		
		// xlog->log("Calls to send binary file\n");
		Socket->Send(Header(GetBinarySize()));	
		Socket->BinarySend(BinaryPacketList);
	} else
	{
		STString GetTextBodyEarlyForSize;
		// xlog->log("About to get MessageBody.Len()\n");
		GetTextBodyEarlyForSize = Body();
		// xlog->log("GetTextBodyEarlyForSize.Len() = %d\n", GetTextBodyEarlyForSize.Len());
		// xlog->log("MessageBody.Len() = %d\n", MessageBody.Len());
		Socket->Send(Header(GetTextBodyEarlyForSize.Len()));	
		Socket->Send(GetTextBodyEarlyForSize);
	}
}

void PageServer::PostBinaryFile(char *FileName)
{
	STString TempStr = FileName;
	PostBinaryFile(TempStr);
}

void PageServer::PostBinaryFile(const STString& FileName)
{	
	// xlog->log("void PageServer::PostBinaryFile(%s)\n", (const char*)FileName);
	// Okay, we know it is a binary already but if the develper of index.so call this function manualy we still need to set the right header.
	SetFileType(FileName);
	
	bool FileFound = false;
	STDriveOps DriveOps;
	FILE *FilePointer;
	STString PathFileName;
	char* Buffer;
	//int BufferIndex;
	int NextChar, Done;
	int BytesSent;	
	struct stBinaryPacket* BPacket;
	//if((*_SERVER)["debugpost"] == "on")
	//{		
	//	xlog->log("void PageServer::PostBinaryFile('%s')\n", (const char*)FileName);
	//}		
	ClearBinaryPacketList();
	PathFileName = (*_SERVER)["DOCUMENT_ROOT"] + FileName;
	if(DriveOps.FileExists(PathFileName))
	{		
		// open the binary file for reading and dump the bytes baby.
		FilePointer = fopen((const char*)PathFileName, "rb");
		if(FilePointer)
		{
			FileFound = true;
			Done = 0;
			while(Done == 0)
			{
				// BPacket = (struct stBinaryPacket*)malloc(sizeof(stBinaryPacket));
				BPacket = new struct stBinaryPacket;
				BPacket->Size = 0;
				BPacket->Data = new char[BINARY_PACKET_SIZE];
				BinaryPacketList->Add(BPacket);
				
				while((BPacket->Size < BINARY_PACKET_SIZE) && (Done == 0))
				{
					NextChar = fgetc(FilePointer);
					if(NextChar == EOF)
					{
						Done = 1;
					} else
					{
						BPacket->Data[BPacket->Size++] = (char)NextChar;
					}
				}
			}
			fclose(FilePointer);
		} 		
	} else
	{
		// Some built in files
		// xlog->log("FileName.Pos(\".png\", -1) = %d\n", FileName.Pos(".png", -1));
		// xlog->log("FileName.Len() = %d\n", FileName.Len());
		if(FileName.Pos(".png", -1) == FileName.Len() - 4)
		{
			xlog->log("look through a list of xpm's\n");
		}			
	}
	if(! FileFound)
	{
		// xlog->log("SetError(HTTP_NOT_FOUND);\n");
		SetError(HTTP_NOT_FOUND);
	}
}

void PageServer::ClearBinaryPacketList()
{
	struct stBinaryPacket* BPacket;
	while(BinaryPacketList->Count() > 0)
	{
		BPacket = (struct stBinaryPacket*)BinaryPacketList->Item(0);
		delete[] BPacket->Data;
		delete BPacket;
		BinaryPacketList->Delete(0);
	}
}

void PageServer::ListDirectory(const char *DirPath)
{
	STString TempStr = DirPath;
	ListDirectory(TempStr);
}

void PageServer::ListDirectory(const STString& DirPath)
{
	// xlog->log("void PageServer::ListDirectory(%s)\n", (const char*)DirPath);
	STDriveOps DriveOps;
	STStringList Contents;
	STString PostStr, DocRoot, ListPath, UrlLink, WorkDirPath;
	STHash ColorHash;
	unsigned int ColorIndex;
	
	WorkDirPath = DirPath;
	
	ColorIndex = 0;
	ColorHash[(unsigned int)0] = "#FFFFFF";
	ColorHash[(unsigned int)1] = "#F4F4F4";
	
	// xlog->log("void PageServer::ListDirectory(%s)\n", (const char*)DirPath);
	DocRoot = (*_SERVER)["DOCUMENT_ROOT"];
	
	if(WorkDirPath.Pos('/', -1) != WorkDirPath.Len() - 1)
	{
		WorkDirPath += "/";
	}
	
	SetFileType(FT_GENERATED);
	Post("<TABLE cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"><TR><TD>");
	Post("<B>Directory listing of " + WorkDirPath + "</B></TD><TD><SMALL>" + (*_SERVER)["SERVER_SOFTWARE"] + "</SMALL></TD></TR></TABLE><HR><BR>\n");
	Post("<TABLE cellspacing='0' cellpadding='0' width='100%'>");
	if(WorkDirPath.Len() > 1 )
	{
		UrlLink = "<A HREF=\"" + WorkDirPath + "..\">";
		Post("<TR BGCOLOR=" + ColorHash[(ColorIndex ^= 1)] + "><TD WIDTH=50><IMG SRC=\"/icons/up.png\"></TD><TD>");
		Post(UrlLink + "Parent Directory</A><BR>\n");
		Post("</TD></TR>");
	}
	ListPath = DocRoot + WorkDirPath;
	// xlog->log("ListPath = '%s'\n", (const char*)ListPath);
	DriveOps.GetDirectories(&Contents, ListPath);
	while(Contents.Count() > 0)
	{
		UrlLink = "<A HREF=\"" + WorkDirPath + Contents.Item(0) + "\">";
		Post("<TR BGCOLOR=" + ColorHash[(ColorIndex ^= 1)] + "><TD WIDTH=50><IMG SRC=\"/icons/folder.png\"></TD><TD>");
		PostStr =  UrlLink + Contents.Item(0) + "</A><BR>\n";
		Post(PostStr);
		Post("</TD></TR>");
		Contents.Delete(0);
	}
	DriveOps.GetFiles(&Contents, ListPath);
	while(Contents.Count() > 0)
	{
		UrlLink = "<A HREF=\"" + WorkDirPath + Contents.Item(0) + "\">";
		Post("<TR BGCOLOR=" + ColorHash[(ColorIndex ^= 1)] + "><TD WIDTH=50><IMG SRC=\"/icons/file.png\"></TD><TD>");
		PostStr = UrlLink + Contents.Item(0) + "</A><BR>\n";
		Post(PostStr);
		Post("</TD></TR>");
		Contents.Delete(0);
	}	
	Post("</TABLE>");
}

void PageServer::Log(const char* NewLogMessage, ...)
{
   va_list argp;  // Create an argument list pointer
	va_start(argp, NewLogMessage);
	STString LogMsg;	
	LogMsg.VaListFormating(NewLogMessage, &argp);
	xlog->log((const char*)LogMsg);
	va_end(argp);
}

void PageServer::HPost(const char *DataString)
{
	STString TempString = DataString;
	HPost(TempString);
}
		
void PageServer::HPost(const STString& DataString)
{
	if(HttpCode == HTTP_OK)
	{
		MessageHeader += DataString;
		SetFileType(FT_GENERATED);
	}
}

void PageServer::IPost(const char *DataString)
{
	STString TempString = DataString;
	IPost(TempString);
}
		
void PageServer::IPost(const STString& DataString)
{
	//xlog->log("void PageServer::IPost(%s)\n", (const char*)DataString);
	//xlog->log("HttpCode = %d\n", HttpCode);
	if(HttpCode == HTTP_OK)
	{
		//xlog->log("HttpCode == HTTP_OK\n");
		MessageBody += DataString;
		SetFileType(FT_INCLUDE_SCRIPT);
	} /*else
	{
		xlog->log("HttpCode != HTTP_OK\n");
	}*/
}

void PageServer::Post(const char *DataString)
{
	STString TempString = DataString;
	Post(TempString);
}
		
void PageServer::Post(const STString& DataString)
{	
	//xlog->log("HttpCode = %d\n", HttpCode);
	if(HttpCode == HTTP_OK)
	{
		//xlog->log("HttpCode == HTTP_OK\n");
		MessageBody += DataString;
		SetFileType(FT_GENERATED);
	} /* else
	{
		xlog->log("HttpCode != HTTP_OK\n");
	} */
}

void PageServer::EPost(const char* DataString, ...)
{
   va_list argp;  // Create an argument list pointer
	va_start(argp, DataString);
	STString DataStringStr;	
	DataStringStr.VaListFormating(DataString, &argp);
	EPost(DataStringStr);
	va_end(argp);
}
		
void PageServer::EPost(const STString& DataString)
{
	if(HttpCode != HTTP_OK)
	{
		MessageBody += DataString;
	}
}

void PageServer::SetFileType(int DirectSetting)
{
	//xlog->log("void PageServer::SetFileType(%s)\n", (const char*)GetMimeType(DirectSetting));
	if((FileTypeValue == FT_NOT_SET) || (FileTypeValue == FT_GENERATED)) // Generators need to be able to change what they serve. 
	{
		//xlog->log("File type now set.\n");
		FileTypeValue = DirectSetting;
	} /*else
	{
		xlog->log("Cannot be set now.\n");
	}*/
}

void PageServer::SetContentType(const char* CustomContentType)
{
	SetFileType(FT_GENERATED);
	STString WorkString = CustomContentType;
	if(WorkString.Pos(":") > -1)
	{
		WorkString.Remove(0, WorkString.Pos(":") + 1);
	}
	
	MimeType = "Content-Type: ";
	MimeType += CustomContentType;
}

void PageServer::SetFileType(const STString& FileName)
{
	//xlog->log("void PageServer::SetFileType(%s)\n", (const char*)FileName);
	STString Extension;
	Extension = FileName;
	Extension.Remove(0, Extension.Pos('.', -1));
		
	time_t  theTime;
	theTime = time((time_t *) NULL);
	theTime += (long) (60*60*24);
	STString Tomorrow;
	Tomorrow.Sprintf("Expires: %s", ctime(&theTime));
	
	// These all have the default mime type of text/html
	if(Extension == ".html")
	{
		SetFileType(FT_TEXT_HTML);
	}		
	if(Extension == ".htm")
	{
		SetFileType(FT_TEXT_HTML);
	}		
	if(Extension == ".so")
	{
		SetFileType(FT_GENERATED);
	}		
	if(Extension == ".bsh")
	{
		SetFileType(FT_GENERATED);
	}		
	if(Extension == ".gif")
	{ 
		MimeType = "Cache-Control: max-age=3600\n";
		MimeType += Tomorrow;
		MimeType += "Age: 1\n";
		MimeType += "Content-Type: image/gif";
		SetFileType(FT_GRAPHIC);
	}
	if(Extension == ".css")
	{
		MimeType = "Cache-Control: public, max-age=3600, must-revalidate, proxy-revalidate\n";
		MimeType += Tomorrow;
		MimeType += "Age: 1\n";
		MimeType += "Content-Type: text/css";
		SetFileType(FT_INCLUDE_SCRIPT);
	}	
	if(Extension == ".js")
	{
		MimeType = "Cache-Control: public, max-age=3600, must-revalidate, proxy-revalidate\n";
		MimeType += Tomorrow;
		MimeType += "Age: 1\n";
		MimeType += "Content-Type: application/x-javascript";
		SetFileType(FT_INCLUDE_SCRIPT);
	}	
	if(Extension == ".jpg")
	{
		MimeType = "Cache-Control: max-age=3600\n";
		MimeType += Tomorrow;
		MimeType += "Age: 1\n";
		MimeType += "Content-Type: image/jpeg";
		SetFileType(FT_GRAPHIC);
	}
	if(Extension == ".jpeg")
	{
		MimeType = "Cache-Control: max-age=3600\n";
		MimeType += Tomorrow;
		MimeType += "Age: 1\n";
		MimeType += "Content-Type: image/jpeg";
		SetFileType(FT_GRAPHIC);
	}
	if(Extension == ".png")
	{
		MimeType = "Cache-Control: max-age=3600\n";
		MimeType += Tomorrow;
		MimeType += "Age: 1\n";
		MimeType += "Content-Type: image/png";		
		SetFileType(FT_GRAPHIC);
	}
	
	if(FileTypeValue == FT_NOT_SET)
	{
		int NextChar;
		bool FileFound = false;
		bool FileContainsExtendedAscii = false;
		STString PathFileName = (*_SERVER)["DOCUMENT_ROOT"] + FileName;
		STDriveOps DriveOps;
		if(DriveOps.FileExists(PathFileName))
		{		
			if((*_SERVER)["debugpost"] == "on")
			{		
				xlog->log("void PageServer::SetFileType('%s')\nScan first 100 bytes looking to identify the file type.", (const char*)FileName);
			}		
			FILE* FilePointer = fopen((const char*)PathFileName, "rb");
			if(FilePointer)
			{
				FileFound = true;
				int Done = 100;
				while(Done-- > 0)
				{				
					NextChar = fgetc(FilePointer);
					if(NextChar == EOF)
					{
						Done = 0;
					} else
					{
						if((NextChar < 0) || (NextChar > 128)) FileContainsExtendedAscii = true;
					}
				}
				fclose(FilePointer);
				if(FileContainsExtendedAscii) 
				{
					MimeType = "Content-Type: binary";
					SetFileType(FT_BINARY);
				} else
				{
					MimeType = "Content-Type: text/plain";
					SetFileType(FT_UNKNOWN_TEXT);
				}
			} else
			{
				SetError(HTTP_NOT_FOUND);
			}				
		} else
		{
			SetError(HTTP_NOT_FOUND);
		}
	}

	//xlog->log("SetFileType = %s\n", (const char*)GetMimeType(FileTypeValue));	
}

STString PageServer::GetMimeType(int MimeType)
{
	ReturnString = "";		
	switch(MimeType)
	{
		case FT_NOT_SET:			ReturnString = "FT_NOT_SET";			break;
		case FT_UNKNOWN_TEXT:	ReturnString = "FT_UNKNOWN_TEXT";	break;
		case FT_TEXT_HTML:		ReturnString = "FT_TEXT_HTML";		break;
		case FT_DIR_LISTING:		ReturnString = "FT_DIR_LISTING";		break;
		case FT_GENERATED:		ReturnString = "FT_GENERATED";		break;
		case FT_GRAPHIC:			ReturnString = "FT_GRAPHIC";			break;
		case FT_INTERNAL:			ReturnString = "FT_INTERNAL";			break;
		case FT_INCLUDE_SCRIPT:	ReturnString = "FT_INCLUDE_SCRIPT";	break;
		case FT_BINARY: 			ReturnString = "FT_BINARY";			break;
	}
	return(ReturnString);
}

void PageServer::ShowVars()
{
	int i, v;
	STHash *TempHash;
	
	//xlog->log("void PageServer::ShowVars()\n");
	Post("<P ALIGN='CENTER'><BIG><B><I>Server variables</I></B></BIG></P>");
	for( v = 0; v < GlobalVars->Count(); v++)
	{
		TempHash = (STHash *)(*GlobalVars)[GlobalVars->Key(v)].ToInt();
		Post("<BR><BR><B><U>" + GlobalVars->Key(v) + "</U></B><BR>");
		for(i = 0; i < TempHash->Count(); i++)
		{
			Post(GlobalVars->Key(v) + "['" + TempHash->Key(i) + "'] = '" + (*TempHash)[TempHash->Key(i)] + "'<BR>\n");			
		}
	}
}

int PageServer::FileType()
{
	return(FileTypeValue);
}

void PageServer::SetError(int NewError)
{
	//xlog->log("void PageServer::SetError(%d)\n", NewError);
	HttpCode = NewError;
	MimeType = "Content-Type: text/html";
	MessageBody = "";
}

void PageServer::Reset()
{
	//xlog->log("void PageServer::Reset()\n");
	FileTypeValue = FT_NOT_SET;
	HttpCode = HTTP_OK;	
	MessageBody = "";
	MessageHeader = "";
	MimeType = "Content-Type: text/html";
	SendCookies = "";
	_COOKIE->Clear();
	ClearBinaryPacketList();
}

void PageServer::SetCookie(const char *CookieString)
{
	SendCookies += "Set-Cookie: ";
	SendCookies += CookieString;
	SendCookies += "\n";
}

void PageServer::SetCookie(const STString& CookieString)
{
	SendCookies += "Set-Cookie: ";
	SendCookies += CookieString;
	SendCookies += "\n";
}

void PageServer::SetCookie(const char *format, va_list* argp)
{
   // Not knowing the number of arguments or the length they will be after being converted we
   // will try to make enough room for them. We will stop if 100k bytes are not enough.
   int ErrorCode;
   int NewBufferSize;
   char* TempStringBuffer;

	bool Done = false;
	NewBufferSize = 1024;
	while(! Done)
	{
		// TempStringBuffer = (char*)malloc(NewBufferSize);
		TempStringBuffer = new char[NewBufferSize + 1];
		
		if ((int)TempStringBuffer > 1)
		{
			ErrorCode = vsnprintf(TempStringBuffer,NewBufferSize,format,*argp);
			if((ErrorCode > - 1) && (ErrorCode < NewBufferSize))
			{
				Done = true;
			} else
			{
				NewBufferSize += 1024;
				delete[] TempStringBuffer;
			}
		}
   }
   
	SendCookies += "Set-Cookie: ";
	SendCookies += TempStringBuffer;
	SendCookies += "\n";
   delete[] TempStringBuffer;
}

STString PageServer::Header(int BodySize)
{
	//STTime Time;
	
	//xlog->log("STString PageServer::Header()\n");
	// HTTP 1.0 do not re-use old sockets
	// HTTP 1.1 will re-use old sockets
	// Currently EBWS will only use a socket once. A future version may support HTTP 1.1
	ReturnString = "HTTP/1.0 ";
	switch(HttpCode)
	{
		case HTTP_BAD_REQUEST:	ReturnString += "400 Bad Request";		break;
		case HTTP_UNAUTHORIZED:	ReturnString += "401 Unauthorized";		break;
		case HTTP_FORBIDDEN:		ReturnString += "403 Forbidden";			break;
		case HTTP_NOT_FOUND:		ReturnString += "404 Not Found";			break;
		case HTTP_ERROR:			ReturnString += "500 Internal Server Error";	break;
		default: ReturnString += "200 OK";
	}
	//ReturnString += "\nDate:" + Time.DateTimeString(0) + "\n";	
	/*STSystemAPI SystemAPI;
	STStringList OutPut;
	SystemAPI.SystemCommand("date +\"%a, %b %d %Y %H:%M:%S %Z\"", &OutPut);	
	ReturnString += "\nDate: " + OutPut.Item(0) + "\n"; */

	time_t  theTime;
	theTime = time((time_t *) NULL);
	STString Today;
	Today.Sprintf("\nDate: %s", ctime(&theTime));
	ReturnString += Today;	
	
	ReturnString += "Server: " + (*_SERVER)["SERVER_SOFTWARE"] + "\n";
	ReturnString += SendCookies;	
	STString ContentLength;
	ContentLength.Sprintf("Conten-Length: %d\n", BodySize);
	ReturnString += ContentLength;
	//ReturnString += "Keep-Alive: timeout=15, max=100\n";	
	//ReturnString += "Connection: Keep-Alive\n";
	
	ReturnString += "Connection: close\n";
	ReturnString += MimeType + "\n\n";
	if((*_SERVER)["debugheader"] == "on")
	{		
		xlog->log("\n\n-------------------- <HEADER> --------------------\n");
		xlog->log("%s", (const char*)ReturnString);
		xlog->log("\n-------------------- </HEADER> -------------------\n\n");
	}
	return(ReturnString);
}
					
STString PageServer::Body()
{
	// xlog->log("STString PageServer::Body()\n");
	STString Header = "";
	
	ReturnString = "";
	if(MessageHeader != "")
	{
		Header = "<HEAD>\n" + MessageHeader + "\n</HEAD>\n";		
	} 
	// xlog->log("FileTypeValue = %s\n", (const char*)GetMimeType(FileTypeValue));
	switch(FileTypeValue)
	{
		case FT_DIR_LISTING:
		case FT_INTERNAL:
		case FT_GENERATED:		ReturnString = "<HTML>" + Header + "<BODY>\n" + MessageBody; 	break;		
		case FT_UNKNOWN_TEXT:
		case FT_TEXT_HTML:			
		case FT_INCLUDE_SCRIPT:		ReturnString = MessageBody;									break;
	}	
	switch(HttpCode)
	{
		case HTTP_BAD_REQUEST:	ReturnString += "<B>Error: 400 Bad Request</B><BR>\n";		break;
		case HTTP_UNAUTHORIZED:	ReturnString += "<B>Error: 401 Unauthorized</B><BR>\n";	break;
		case HTTP_FORBIDDEN:		ReturnString += "<B>Error: 403 Forbidden</B><BR>\n";		break;
		case HTTP_NOT_FOUND:		ReturnString += "<B>Error: 404 Not Found</B><BR>\n";		break;
		case HTTP_ERROR:			ReturnString += "<B>Error: 500 Internal Server Error</B><BR>\n";	break;
	}
	switch(FileTypeValue)
	{
		case FT_INCLUDE_SCRIPT:		break;
		case FT_DIR_LISTING:
		case FT_INTERNAL:
		case FT_GENERATED:		ReturnString += "\n</BODY></HTML>\n";	break;
	}
	// xlog->log("_SERVER['debugpost'] = '%s'\n", (const char*)((*_SERVER)["debugpost"]));
	if((*_SERVER)["debugpost"] == "on")
	{		
		xlog->log("\n\n-------------------- <POST BODY> --------------------\n");
		xlog->log("%s", (const char*)ReturnString);
		xlog->log("\n-------------------- </POST BODY> -------------------\n\n");
	}
	return(ReturnString);		
}

bool PageServer::FileUploaded()
{
	// struct stBinaryPacket* BPacket;
		
	// Use to save in memory, don't do it this way any more. Working toward making it work like other servers.
	/*STList* RxBufferList;
	xlog->log("S _FILE = 0x%08X\n", _FILE);
	RxBufferList = (STList*)(*_FILE)["BUFFER_LIST_POINTER"].ToInt(); */
	
	if((*_SERVER)["debuguploadinfo"] == "on")
	{		
		xlog->log("Temp file name = '%s'\n", (const char*)(*_FILE)["TEMP_FILE_NAME"]);
		xlog->log("File name = '%s'\n", (const char*)(*_FILE)["FILE_NAME"]);
		xlog->log("File size = '%s'\n", (const char*)(*_FILE)["SIZE"]);
	}
	return((*_FILE)["TEMP_FILE_NAME"].Len() > 0);
}

bool PageServer::SaveUploadedFile(const STString& NewFilePathName)
{
	bool Success = false;
	FILE* SrcFilePointer;
	FILE* TrgFilePointer;
	SrcFilePointer = fopen((const char*)(*_FILE)["TEMP_FILE_NAME"], "rb");
	TrgFilePointer = fopen((const char*)NewFilePathName, "wb");
	if(SrcFilePointer && TrgFilePointer)
	{
		unsigned int ReadChar = fgetc(SrcFilePointer);
		while(ReadChar != EOF)
		{
			fputc(ReadChar, TrgFilePointer);
			ReadChar = fgetc(SrcFilePointer);
		}
		fclose(SrcFilePointer);
		fclose(TrgFilePointer);
		Success = true;
	} 
	return(Success);
	/*STString CommnadString;
	CommnadString.Sprintf("mv \"%s\" \"%s\"", (const char*)(*_FILE)["TEMP_FILE_NAME"], (const char*)NewFilePathName);
	_FILE->Clear();
	(*_FILE)["MAX_FILE_SIZE"] = "8192";
	return(system((const char*)CommnadString) == 0);*/
	
}
