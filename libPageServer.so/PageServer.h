#ifndef included_PageServer
#define included_PageServer

#include <stdarg.h>
#include <STString.h>
	
// Error Codes
#define HTTP_OK				200
#define HTTP_BAD_REQUEST 	400
#define HTTP_UNAUTHORIZED	401
#define HTTP_FORBIDDEN		403
#define HTTP_NOT_FOUND		404
#define HTTP_ERROR			500

// File Types
#define FT_NOT_SET 			0
#define FT_UNKNOWN_TEXT 	1
#define FT_TEXT_HTML 		2
#define FT_DIR_LISTING		3
#define FT_GENERATED 		4
#define FT_GRAPHIC 			5
#define FT_INTERNAL			6
#define FT_INCLUDE_SCRIPT	7
#define FT_BINARY				8
#define FT_CUSTOM				9

class STHash;
class SocketServer;
class STList;
class STDynamicDataLink;
class STXlog;

/**
 * For more information see /var/www/About_Ebws.html
 * or
 * http://www.silicontao.com/software/ebws/About_Ebws.html
 */

class PageServer
{
	public:
		PageServer(SocketServer *SocketPorinter, STHash *PGlobalVars, STXlog *NewXlog);
		~PageServer();	
		/**
		 * Sends the contents of a binary file like an image or download as a raw binary to the client viewer.
		 */ 	
		void PostBinaryFile(char *FileName);
		void PostBinaryFile(const STString& FileName);

		/**
		 *	Post normal text as HTML header. Format is set to HTML.
	    */	
		void HPost(const STString& DataString);
		void HPost(const char *DataString);
	
		/**
		 * Set a custom content-type in the HPost.
		 */
		void SetContentType(const char* CustomContentType);	

		/**
		 *	Post normal text as included. Sets format to included so no formating data is sent.
	    */	
		void IPost(const char *DataString);
		void IPost(const STString& DataString);
	
		/**
		 *	Post normal text as HTML body. Format is set to HTML.
	    */	
		void Post(const char *DataString);
		void Post(const STString& DataString);
	
		/**
		 *	Used to set a cookie on the client browser. This function has no smarts to help with proper cookie formating. You must give it a complete cookie string that is well formated.
		 * @li
		 * Example call
		 * <PRE>
		 * SetCookie("field3=My+domain+is+very+cool; expires=Wed, 03 Oct 2007 02:13:49 GMT; path=/path/to/thing/; domain= www.SiliconTao.com");
		 * </PRE>
		 */
		void SetCookie(const char *CookieString);
		void SetCookie(const STString& CookieString);
		
		/**
		 * Provided for ease of use will accept an inline formated array of arguments.
		 * @li
		 * Example call
		 * <PRE>
		 * SetCookie("field3=%s; expires=%s; path=%s; domain=%s", (const char*)StringVar1, (const char*)Date2Str(now()), (const char*)AppPath, (const char*)OurDomainName);
		 * </PRE>
		 * Inside the local wrapper for this call it is defined as void SetCookie(const char *format, ...); and should be used in this way.
		 */
		void SetCookie(const char *format, va_list* argp);
		
		/**
		 *	Post extra information about the error after an error has happened in the same page load event. You must first call @ref HttpError to set the HTML error code. See also Error Codes and SetError.
	    */
		void EPost(const char *DataString, ...);
		void EPost(const STString& DataString);
	
		/**
		 * Log messages to the syslogd server. If EBWS is run in debug mode text is also output to STDOUT.
		 */
		void Log(const char*NewLogMessage, ...);

		/**
		 * Returns a pointer the STDynamicDataLink that is running in EBWS. Only one instance of STDynamicDataLink is able to run per program. EBWS provides
		 * this service to enable inter application and system communication. This link is not serviced very often so it can only be used to ask questions. 
		 * If the DataLink is never used it will not be loaded, it will only be loaded the first time a question is asked.
		 */
		STDynamicDataLink* GetDataLink();

		/**
		 * Developers of custom PageOjbect content do not use this function.
		 * It is publicly available to be used by other objects internally to the funciton of EBWS only.
		 */
		void Reset();

		/**
		 * Developers of custom PageOjbect content do not use this function.
		 * It is publicly available to be used by other objects internally to the funciton of EBWS only.
		 */
		STString Header(int BodySize);

		/**
		 * Developers of custom PageOjbect content do not use this function.
		 * It is publicly available to be used by other objects internally to the funciton of EBWS only.
		 */
		STString Body();

		/**
		 * Developers of custom PageOjbect content do not use this function.
		 * It is publicly available to be used by other objects internally to the funciton of EBWS only.
		 */
		void SetFileType(const STString& FileName);
		void SetFileType(int DirectSetting);

		/**
		 * Developers of custom PageOjbect content do not use this function.
		 * It is publicly available to be used by other objects internally to the funciton of EBWS only.
		 */
		int FileType();

		/**
		 * Generate an error for display to the viewing client. Any data currently in the post buffer waiting to be sent to
		 * the the viewing client will be lost. The viewing client will only receive the error page. See also Error Codes and EPost.
		 */
		void SetError(int NewError);

		/**
		 * Displays the content of the global variables as an HTML formated page to the viewing client.
		 */
		void ShowVars();

		/**
		 * Displays the content of a directory as an HTML formated page to the viewing client.
		 */
		void ListDirectory(const char *DirPath);
		void ListDirectory(const STString& DirPath);

		/**
		 * Developers of custom PageOjbect content do not use this function.
		 * It is publicly available to be used by other objects internally to the funciton of EBWS only.
		 */
		void PostPage();

		/**
		 * Displays the content of static text file as an HTML formated page to the viewing client.
		 */
		void PostFromFile(const STString& FileName);

		/**
		 * Included Posting. Displays the content of static text file as HTML formated sub page content to the viewing client.
		 * Does not try to generate any standard HTML headers. Assumes that has already been done. Use this to include
		 * CSS or JavaScript file in the HTML page.
		 */
		void IPostFromFile(const STString& FileName);

		/**
		 * Works like @ref PostFromFile but parses the file for known global variables and replaces them with known values. Must use @ref Endorse to register custom variables first.
		 */
		void EvalFile(const STString& FileName);
		
		/**
		 * Works like @ref EvalFile but parses posted data in memory buffer that is ready to be sent out to the client.
		 */
		void EvalPosted();

		/**
		 * Developers of custom PageOjbect content do not use this function.
		 * It is publicly available to be used by other objects internally to the funciton of EBWS only.
		 */
		void Replace(STString* RebuildString, const STString& Match, const char* NewStr);
		void Replace(STString* RebuildString, const char* Match, const char* NewStr);
		void Replace(STString* RebuildString, const char* Match, const STString& NewStr);
		void Replace(STString* RebuildString, const STString& Match, const STString& NewStr);

		/**
		 * Works like PostFromFile but wraps the contents in HTML PRE tags and formats special symbols for original viewing.
		 * For example the < symbol is replaced by &lt; Use this to show HTML and other source code.
		 */
		void PostFromFilePre(const STString& FileName);

		/**
		 * Required to create and use custom STHash variables globally. Hash variables must be endorsed before they will be identified by EvalFile.
		 */
		void Endorse(STHash *HashPointer, const char *HashName);
		void Endorse(STHash *HashPointer, const STString& HashName);
		
		/** 
		 * Used to remove global STHash variables when they are no longer needed.
		 */
		void Disavow(const char *HashName);

		/**
		 * EBWS currently supports the FORM uploading of one file at a time. This function returns true if a file was uploaded by the viewing client.
		 * Look to the values stored in the global variable _FILE to find details about the uploaded file.
		 */
		bool FileUploaded();

		/**
		 * Saves the uploaded file to the giving file name and clears and frees memory used by the global variable _FILE.
		 */
		bool SaveUploadedFile(const STString& NewFilePathName);
	
		/**
		 * Loads a shared object file of a given name then executes standard EBWS calls to it. Can retrun an int but it has a custom meaning not an error value.
		 */
		int ExecuteLibrary(const STString& LibName);

		STString ReturnString;
		STString MimeType;
		
		/**
		 * These global variables are for use by developers of custom PageOjbect content.
		 * They are string hash objects from the SiliconTaoCppLibs library.
		 */
		STHash *GlobalVars, *_SERVER, *_GET, *_POST, *_COOKIE, *_GLOBAL, *_FILE;
		
	private:
		STDynamicDataLink* Link;	
		SocketServer *Socket;
		int HttpCode;
		int FileTypeValue;
		STList *ModuleList;	
		STString MessageBody, MessageHeader, SendCookies;	
		STList* BinaryPacketList;
		int GetBinarySize();
		STXlog *xlog;
	
		void ClearBinaryPacketList();
		STString GetMimeType(int MimeType);
};


#endif // included_PageServer
